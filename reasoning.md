# Reasoning Notes

## Methods of reasoning

- __testimony__: accepting someone's words as evidence
- __observation__: acquiring information through one's senses
- __argument__: a series of propositions (the _predicates_) in support of a
  claim (the _conclusion_)

## Graham's hierarchy of disagreement

1. _refuting the central point_
1. _refutation_: quoting someone and explaining why they are mistaken
1. _counterargument_: contradiction with reasoning
1. _contradiction_: stating the opposite
1. _responding to tone_: criticising the style of language not the content
1. _ad hominem attack_: questioning the authority of the person
1. _name-calling_: attacking the person

## Elements of arguments

- __truth value__: the state of being true or false (or indeterminate)
- __assertion__: a linguistic act that has a truth value
- __proposition__: the content of an assertion, i.e. a statement that has truth
  value
- __predicate__: a statement containing one or more variables; if truth values
  are assigned to all the variables in a predicate, the resulting statement is a
  proposition

## Argument types

- __deductive__: if all premises are true and there is entailment then the
  conclusion is necessarily true
    - __detachment / Modus Ponens__: _P → Q, P ⊨ Q_
    - __syllogism / Modus Barbara__: _P → Q, Q → R ⊨ P → R_
    - __contrapositive / Modus Tollens__: _P → Q, ¬Q ⊨ ¬P_
    - __Modus Tollendo Ponens__: _P ∨ Q, ¬P ⊨ Q_
- __inductive__: premises provide strong evidence to predict a conclusion that
  is likely to be true
- __abductive__: conclusion isn't based on evidence but on eliminating possible
  explanations until the most plausible explanation is left

## Reasoning errors

- __non sequitur__: "does not follow"; a conclusion is erroneously derived from
  premises in a deductive argument
    - __affirming the consequent__: _P → Q, Q ⊨ P_
    - __denying the antecedent__: _P → Q, ¬P ⊨ ¬Q_

## Fallacies

- __faulty generalisation__: reaching a conclusion from weak premises
    - __fallacy of accident__: ignoring an exception to a general claim
    - __cherry-picking__: presenting the cases that confirm a claim while
      ignoring the ones that contradict it
    - __anecdotal evidence__: using an isolated example to prove a general
      statement
    - __bandwagon__: assuming that if many people believe something then it's
      true
    - __middle ground__: assuming that a compromise between two extremes must be
      the truth
    - __gambler's fallacy__: assuming causation between independent events
    - __false cause__: assuming that correlation implies causation
    - __slippery slope__: assuming that if we allow A to happen then it will
      eventually lead to B, therefore we shouldn't allow A to happen
    - __black-or-white__: presenting two alternatives as the only possibilities
      while many more exist
- __red herring__: reaching a conclusion from deliberately distracting premises
    - __ad hominem__: attacking the arguer instead of the argument
    - __appeal to authority__: assuming that if someone with great reputation
      claims something then it's true
    - __strawman__: deliberately misrepresenting someone's argument
    - __appeal to emotion__: an emotionally manipulative response instead of a
      valid argument
    - __tu quoque__ (you too): answering criticism with criticism instead of a
      counterargument
    - __missing the point__: a valid argument that doesn't address the issue in
      question
    - __personal incredulity__: assuming that because something is difficult to
      understand, it's probably false
    - __loaded question__: a question that can only be answered one way without
      appearing guilty
    - __begging the question__: an argument in which the premise already assumes
      that the conclusion is true
- __ambiguity fallacy__: applying double meaning to misrepresent the truth
    - __equivocation__: using multiple different meanings of a word in the
      premises, leading to a false conclusion
    - __fallacy of composition__: assuming that if something is true for one
      part then it's true for the whole
    - __fallacy of division__: assuming that if something is true for the whole
      then it must be true for all parts
- __psychologist's fallacy__: assuming that a subjective experience is generally
  true
    - __special pleading__: making up exceptions to a rule that cannot be proved
      or disproved
    - __burden of proof__: assuming that because a claim cannot be either proved
      or disproved by anyone then it must be true
