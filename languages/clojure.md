# Clojure Reading List

## Books

- [Clojure for the Brave and True](https://www.braveclojure.com/foreword/)
- [Clojure Cookbook](https://github.com/clojure-cookbook/clojure-cookbook)

## Tutorials

- [Clojure by Example](https://kimh.github.io/clojure-by-example/)
- [Data Structures Tutorial](https://purelyfunctional.tv/guide/clojure-collections/)
- [Concurrency Tutorial](https://purelyfunctional.tv/guide/clojure-concurrency/)
- [Clojure Style Guide](https://guide.clojure.style/)

## Advanced topics

- [Import system](https://8thlight.com/blog/colin-jones/2010/12/05/clojure-libs-and-namespaces-require-use-import-and-ns.html)
- [Logging](https://lambdaisland.com/blog/2020-06-12-logging-in-clojure-making-sense-of-the-mess)
- [S-expressions](http://www.gigamonkeys.com/book/syntax-and-semantics.html)
- [Lazy Sequences](http://theatticlight.net/posts/Lazy-Sequences-in-Clojure/)
- [Error Handling](https://adambard.com/blog/acceptable-error-handling-in-clojure/)
- [Software Transactional Memory](https://www.sw1nn.com/blog/2012/04/11/clojure-stm-what-why-how/)
- Persistent Vectors
  [pt.1](https://hypirion.com/musings/understanding-persistent-vector-pt-1),
  [pt.2](https://hypirion.com/musings/understanding-persistent-vector-pt-2),
  [pt.3](https://hypirion.com/musings/understanding-persistent-vector-pt-3)
- [Spec](https://www.pixelated-noise.com/blog/2020/09/10/what-spec-is/)
- [`core.async` Channels](https://www.infoq.com/presentations/clojure-core-async/)
  (presentation)

## Background videos

- [Clojure in 10 Big Ideas](https://vimeo.com/223240720)
- [Are We There Yet?](https://www.infoq.com/presentations/Are-We-There-Yet-Rich-Hickey/)
- [Simple Made Easy](https://www.infoq.com/presentations/Simple-Made-Easy/)
- [Design, Composition and Performance](https://www.infoq.com/presentations/Design-Composition-Performance/)

## Community

- [Clojure Subreddit](https://www.reddit.com/r/Clojure/)
- [Clojurians Slack](http://clojurians.net/)
- [London Clojurians Meetup](https://www.meetup.com/London-Clojurians/)
