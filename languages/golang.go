/* Golang Peculiarities */

// Every file starts with package declaration
package golang

import (
	"fmt"
	"reflect"
	"sync"
	"time"

	. "math" // '.' imports all exported identifiers into the local namespace

	// Modules can be imported directly from GitHub and installed with `go get`
	log "github.com/sirupsen/logrus"
)

const (
	// Identifiers whose names start with capital letters are visible outside
	// the package; others are package-local
	X int = iota // `iota` assigns incrementing numbers to constants
	y
)

func assert(expr bool) {
	if !expr { // Brackets are optional
		log.Fatal("Assertion error")
	}
}

// `init` runs when the package is imported
func init() {
	assert(X == 0 && y == 1)
}

// Comments right above a declaration are documentation comments
type Element struct {
	// Tag strings can be assigned to structure fields
	name  string `key1:"value1" key2:"value2"`
	value int
}

// Functions can be bound to a type called a "receiver"; they can
// access the bound variable as if it was passed in as an argument
func (e *Element) ToString() string {
	return fmt.Sprintf("%s: %d", e.name, e.value)
}

// An interface defines function signatures; any type that binds to all defined
// functions implicitly implements the interface
type CustomStringer interface {
	ToString() string
}

// Union types can be declared with an interface
type Number interface {
	int64 | float64
}

// Functions are first class citizens
func useCallback(f func(int) bool, n int) bool {
	return f(n)
}

// Functions can be generic and receive variable arguments with the `...`
// operator (like in Python)
func sumIntsOrFloats[T Number](values ...T) T {
	var sum T
	for _, v := range values {
		sum += v
	}
	return sum
}

// Type constraints may be used as type parameters, e.g. a type is `comparable`
// if it supports `==` and `!=` comparisons
func isEqual[T comparable](x T, y T) bool {
	return x == y
}

// `main` is the entry point of the application
func main() {
	// `:=` walrus operator is used for type inference
	length := 3

	// Dynamic array (known as a "slice" in Go)
	vals := make([]int, length)
	assert(vals[0] == 0) // The default value of an `int` is 0

	// `append` doesn't mutate the slice
	vals = append(vals, 1)
	assert(len(vals) == 4)

	// Static array
	moreVals := [3]int{0, 1, 2}

	// Converting array to slice with the `:` range operator (similar to Python)
	assert(len(vals[1:3]) == 2)

	// A 'classic' for loop (like in C)
	for i := 0; i < len(vals); i++ {
		assert(vals[i] == i)
	}

	// A while loop is a for loop with only the condition expression specified
	i := 0
	for i < len(vals) {
		assert(vals[i] == i)
		i++
	}

	// `range` helps iterating over an array
	for i, val := range vals {
		assert(val == i)
	}

	// Anonymous functions can be defined inline
	assert(useCallback(func(n int) bool { return n % 2 == 0 }, 3))

	// `*` is the indirection operator and `&` is the address-of operator
	// (like in C)
	elem := Element{name: "foo", value: 1}
	func(e *Element) {
		e.value = 2
	}(&elem)
	assert(elem.value == 2)

	// Structures can be passed into functions by value, in this case a copy is
	// made of the structure
	func(e Element) {
		e.value = 3
	}(elem)
	assert(elem.value == 2)

	// Tag strings of a structure field can be read using reflection
	elemType := reflect.TypeOf(elem)
	field := elemType.Field(0)
	assert(field.Tag.Get("key1") == "value1")

	elem2 := &Element{name: "bar", value: 2}
	// `Element` implements the `CustomStringer` interface because it has a
	// `ToString` bound function
	func(cs CustomStringer) {
		// A function with a receiver can be called as if it was a method of
		// the receiver
		assert(cs.ToString() == "bar: 2")
	}(elem2)

	// Associative arrays are called "maps"
	dict := map[string]int{"bar": 1}

	// Accessing an element returns two values
	value, hasValue := dict["foo"]
	assert(!hasValue && value == 0) // 0 is the initial value of int type
	value, hasValue = dict["bar"]
	assert(hasValue && value == 1)

	// An "empty interface" may hold values of any data dype
	var iface interface{} = "foobar"
	// "type assertions" can be used to convert to the desired type
	str, isStr := iface.(string)
	assert(isStr && str == "foobar")
	num, isNum := iface.(int)
	assert(!isNum && num == 0) // 0 is the initial value of int type

	// A "channel" is a thread-safe FIFO buffer
	elemChan := make(chan Element, 2) // Buffered, with a 2-element buffer
	truthChan := make(chan bool)      // Unbuffered

	// `cap` gives the buffer size, `len` gives the current number of elements
	assert(cap(elemChan) == 1)
	assert(len(elemChan) == 0)

	// "goroutines" are functions launched in the background in lightweight
	// threads (not native OS threads!)
	go func() {
		// Reading from a channel blocks until a message arrives from the channel
		e := <-elemChan
		assert(e.name == "foo")

		// `select` blocks until a message arrives from either of the channels
		select {
		case <-elemChan:
			assert(false)
		case t := <-truthChan:
			assert(t == true)
		}
	}()

	// Writing to a channel blocks until the channel has a free slot
	elemChan <- elem

	// Use a goroutine for non-blocking write to a channel
	go func() { truthChan <- false }()

	// Use `select` with a `default` block for non-blocking read from a channel
	select {
	case t := <-truthChan:
		assert(t == false)
	default:
		assert(false)
	}

	// Reading from a channel has a 2nd return value that tells if the channel
	// has been closed
	close(elemChan)
	e, isChannelOpen := <-elemChan
	assert(!isChannelOpen)

	// `Defer` registers a function to be executed when the containing function
	// returns
	wg := sync.WaitGroup{}
	go func() {
		wg.Add(1)
		defer wg.Done()
		time.Sleep(2 * time.Second)
	}()
	wg.Wait() // Waits 2 seconds

	// Type arguments are defined in `[]` brackets
	assert(sumIntsOrFloats[int64](1, 2) == 3)
	assert(sumIntsOrFloats[float64](1.0, 2.0, 3.0) == 6.0)
	assert(isEqual[string]("a", "a"))
}
