/*******************************************
 * JavaScript (ECMAScript 5) Peculiarities *
 *******************************************/

/**
 * Block comments that start with exactly two stars
 * are parsed as documentation comments by JSDoc
 */

// 'CommonJS' import system
var assert = require('assert');
module.exports = {assert: assert};

// Directive to raise error if an undeclared variable is used (like in Perl)
'use strict';

// Two types of equality
assert(1 == '1');  // Equality of value with implicit conversion
assert(1 === 1);  // Equality of type and value
assert(1 !== '1');

// Only value types can be compared with relational operators
assert([1, 2] !== [1, 2]);

// One numeric type exists that is floating point
assert(typeof 1 === 'number' && typeof 1.1 === 'number');

// Type conversions
assert(parseInt('1') === 1);
assert(parseFloat('1') === 1.0);
assert((1).toString() === '1');
assert(!!0 === false && !!1 === true);

// `String`, `Number` and `Boolean` as constructors box primitive types;
// as functions they can be used for conversions
assert(
    typeof 1 === 'number' &&
    typeof Number('1') === 'number' &&
    typeof new Number('1') === 'object'
);
assert(
    typeof '1' === 'string' &&
    typeof String(1) === 'string' &&
    typeof new String(1) === 'object'
);
assert(
    typeof true === 'boolean' &&
    typeof Boolean(1) === 'boolean' &&
    typeof new Boolean(1) === 'object'
);

// Array length isn't read-only
var list = [1, 2];
list.length++;
list.push(4);
assert(list[2] === undefined && list[3] === 4);

// The `Array` constructor should be avoided
assert(new Array(3)[0] === undefined);
assert(new Array('3')[0] === '3');

// Such as the `Object` constructor
assert(new Object(1) instanceof Number);
assert(new Object('1') instanceof String);

// Functions are first-class citizens:
function namedFunction(name) {
    console.log(name);
}
// is a syntactic sugar for
var namedFunction = function(name) {
    console.log(name);
};
// and
namedFunction('Foo');
// is equivalent to
(function(name) { console.log(name); })('Foo');
// the above is called an "immediately-invoked function expression"
// (anonymous functions are called "function expressions" in JS)

// Top-level variables are all properties of `global` in Node.js and `window`
// in a web browser
var glob = 'glob';
assert(glob === global.glob);

// Declaring variables with `var` sets the scope to function-local
(function() { var glob = 'al'; })();
assert(glob === 'glob');

(function() { glob = 'al'; })();  // `var` is missing
assert(glob === 'al');  // !

// For loops
for (var i = 0; i < list.length; i++) {
    console.log(i + ': ' + list[i]);
}

// Can also be implemented with array methods and callback functions
list.forEach(function(element, i) {
    console.log(i + ': ' + element);
});

// Closures:
var functions = [];
for (var i = 0; i < 5; i++) {
    functions.push(function() { return i; });
}
// `i` is looked up at the time of calling, not at the time of definition!
assert(functions.every(function(f) {
    return f() === 4;
}));

// Workaround: an immediately executing wrapper function
functions = [];
for (var i = 0; i < 5; i++) {
    functions.push(
        (function(e) {
            return function() { return e; };
        })(i)
    );
}
assert(functions[0]() === 0);

// Objects are hash-maps (dictionaries)
var dict = {
    x: 1,
    getX: function() {
        return this.x;
    },
};
assert(dict['x'] === dict.x);
assert(dict['getX']() === dict.getX());

// Keys are converted to string
var dict2 = { 1: 'A' };
assert(typeof Object.keys(dict2)[0] === 'string');

// `this` refers to the context of a function, not the containing object
var getThis = function() {
    return this;
};
var obj = {
    getThis: getThis,
    getThisNested: function() {
        return getThis();
    },
};
assert(getThis() === global);
assert(obj.getThis() === obj);
assert(obj.getThisNested() === global);  // !

// Call a function in a different context
assert(getThis.call(dict) === dict);
// A context can be bound to a function permanently
var getObj = getThis.bind(obj);
assert(getObj() === obj);

// Variadic functions can access their arguments through `arguments`
function varArgs() {
    return arguments;
}
assert(varArgs(1, 2)[0] === 1);

// A function can be called with fewer arguments than parameters,
// in that case the unassigned parameters become `undefined`
function getArg(x) {
    return x;
}
assert(getArg() === undefined);

// It isn't possible to catch exceptions by type
try {
    throw new Error('Error!');
} catch(anyError) {
    if (!(anyError instanceof Error)) {
        assert(false);
    }
}

// Objects are created with constructor functions from prototypes
var Point = function(x, y) {
    this.x = x;
    this.y = y;
};

Point.prototype = {
    z: 0,
};

var poi = new Point(2, 1);  // `new` makes `Point` a constructor
assert('x' in poi && 'y' in poi && 'z' in poi);
assert(poi.x === 2 && poi.y === 1 && poi.z === 0);

// `x`, `y` are the object's own properties but `z` isn't
assert(poi.hasOwnProperty('x') && poi.hasOwnProperty('y'));
assert(!poi.hasOwnProperty('z'));

// Therefore `z` is looked up from the prototype!
Point.prototype.z = 1;
assert(poi.z === 1);

// `Object` is the prototype of everything (except for `null` and `undefined`)
assert(poi.n === undefined);

Object.prototype.n = 0;
assert(poi.n === 0);
assert((42).n === 0);
