/***************************
 * JavaScript New Features *
 ***************************/

/*** ECMAScript 6 (ES2015) ***/

// 'ES' import system
import { assert } from 'assert';
export default function publicFunction() { return 'foo'; }
export let publicVariable = 'bar';

// Variable scopes (like `def` and `let` in Clojure)
var x = 1;  // function-local scope
if (true) {
    let x = 2;  // block-local scope
    assert(x === 2);
}
assert(x === 1);

// Constants
const c = 'c';
try {
    c = 'd';
} catch(err) {
    assert(err instanceof TypeError);
}

// Lambda functions (called "arrow functions")
const squares = [1, 2, 3].map((x) => x*x);
squares.forEach((x, i) => assert(x === [1, 4, 9][i]));

// Computed properties in objects with `[]`
const key = 'foo';
const map = { [key]: 1 };
assert(map.foo === 1);

// In regular functions `this` is bound to the context
// the function is called with
function createObjectWithoutArrowFunction() {
    this.foo = 21;
    return {
        foo: 42,
        getFoo: function() {
            return this.foo;
        },
    };
}

assert(createObjectWithoutArrowFunction().getFoo() === 42);

// Arrow functions don't have their own `this` and `arguments` bindings
function createObjectWithArrowFunction() {
    this.foo = 21;
    return {
        foo: 42,
        getFoo: () => this.foo,
    };
}

assert(createObjectWithArrowFunction().getFoo() === 21);  // `this` from the outer context

// Spread operator (like `*args` in Python)
const arr1 = [2, 3];
assert([1, ...arr1][2] === 3);

// Literal string interpolation (like `#{var}` in Ruby)
const w = 'world';
assert(`Hello ${w}` === 'Hello world');

// For loop over values
for (let i in ['x']) {  // `in` iterates over keys
    for (let j of ['a']) {  // `of` iterates over values
        assert(i === 0 && j === 'a');
    }
}

// Generators (just like in Python)
function* squares(low, high) {
    while (low <= high) {
        yield Math.pow(low++, 2);
    }
}

for (let n of squares(1, 5)) {
    console.log(n);
}

// Proxy objects for observing property access and assignment of a target object
const target = {
  name: 'Joe',
};

const proxy = new Proxy(target, {
  get(obj, prop) {
    console.log('Getting prop', prop, 'with value', obj[prop]);
    return obj[prop];
  },
  set(obj, prop, value) {
    if (typeof value !== 'string') {
      return false;
    }
    obj[prop] = value;
    return true;
  },
});

assert((proxy.name = 'Jane') === true);
assert((proxy.name = 42) === false);

// Proper classes (they actually get converted into constructor functions)
class Parent {
    constructor(a) {
        this._a = a;
    }

    // Getter/setter
    get a() {
        return this._a.toUpperCase();
    }

    set a(value) {
        this._a = value;
    }
}

class Child extends Parent {
    constructor(a, b) {
        super(a);
        this._b = b;
    }
}

let instance = new Child('a', 'b');
assert(instance.a === 'A');
assert(instance._b === 'b');  // Still no information hiding (like in Python)

// Promises
function getRandomOddNumber() {
    return new Promise((resolve, reject) => {
        let number = parseInt(Math.random() * 100);  // A slow operation

        if (number % 2 === 1) {
            resolve(number);
        } else {
            reject('Not accepted');
        }
    });
}

function getSquare(number) {
    return new Promise((resolve, reject) => resolve(number * number));
}

// Chained together
let promise = getRandomOddNumber()
    .then(getSquare)  // the `resolve` callback of `getRandomOddNumber`
    .then(console.log)  // the `resolve` callback of `getSquare`
    .catch(console.log);  // the `reject` callback of either

// It isn't possible to dereference a future value
// (there is no blocking operation; everything is handled via callbacks)
let value;
promise.then((result) => value = result);

/*** ECMAScript 7 (ES2016) ***/

// Array membership check
assert([1, 2, 3].includes(2));


/*** ECMAScript 8 (ES2017) ***/

// Async functions return promises
async function getFoo() {
    return 'foo';
}

getFoo().then((result) => assert(result === 'foo'));

// Is equivalent to
function getBar() {
    return Promise.resolve('bar');
}

// Or as an arrow function
const getBaz = async () => 'baz';

// Await dereferences a promise; only works inside an async function
(async () => {
    const result = await Promise.all([getFoo(), getBar()]).join('');
    assert(result === 'foobar');
})();


/*** ECMAScript 9 (ES2018) ***/

// Array destructuring
const arr2 = ['A', 'B', 'C'];
const [a1, b1] = arr2;
assert(a1 === 'A' && b1 === 'B');
const [a2, , c2] = arr2;  // ',' skips elements
assert(a2 === 'A' && c2 === 'C');

// Object destructuring
const obj = {one: 1, two: 2, three: 3};
const {one, three} = obj;
assert(one === 1 && three === 3);


/*** ECMAScript 11 (ES2020) ***/

// Conditional chaining with `?.`
const empty = {};

try {
    console.log(empty.foo.bar);
} catch(err) {
    assert(err instanceof TypeError);
}

assert(empty.foo?.bar === undefined);

// Nullish coalescing operator `??`
// Returns the right-hand side if the left-hand side is `null` or `undefined`
assert(null ?? 'hello' === 'hello');
assert(false ?? 'hello' === false);
// `||` returns the right-hand side if the left-hand side is falsy
assert(false || 'hello' === 'hello');

// BigInt type with `n` postfix for literals
assert(typeof 123n === 'bigint');
assert(123n !== 123);


/*** ECMAScript 12 (ES2021) ***/

// Private members in classes with `#`
class InfoHider {
    #foo = 5;

    getFoo() {
        return this.#foo;
    }
}

/*** ECMAScript 13 (ES2022) ***/

// Method for indexing that allows negative indices
assert(arr1[0] === arr1.at(0));
assert(arr1[arr1.length - 1] === arr1.at(-1));

/*** ECMAScript 14 (ES2023) ***/

// Clone array with an element replaced
const arr3 = arr2.with(1, 'D');
assert(arr2[1] === 'B' && arr3[1] === 'D');
