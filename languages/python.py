"""Python Peculiarities"""

import asyncio
import contextlib
import functools
import sys
from typing import Callable, List, Self, Sequence

#   __________________
# _| Variable scoping |________________________________________________________

# Static variables

class Class:
    var = 3

obj = Class()
obj.var = 4
assert obj.var == 4    # instance variable, always a copy
assert Class.var == 3  # static (class) variable

# Global vs. local variables

glob = "glob"

def assign_local():
    glob = "al"

def assign_global():
    global glob
    glob = "al"

assign_local()
assert glob == "glob"
assign_global()
assert glob == "al"

#   ________________________
# _| String / list mutation |__________________________________________________

# `+` creates a new string with every single `+` operation,
# `join` creates one new string (faster)
str1, str2, str3 = "x", "y", "z"
assert str1 + str2 + str3 == "".join([str1, str2, str3]) == "xyz"

# `+` creates a new list, `append` modifies an existing one (faster)
lst1, lst2 = ["x"], ["y"]
assert lst1 + lst2 == ["x", "y"] and lst1 == ["x"]
lst1.append(*lst2)
assert lst1 == ["x", "y"]

#   ___________________
# _| Default arguments |_______________________________________________________

def get_array(array=[]):
    array.append("x")
    return array

# The default value is evaluated when the function is defined,
# not when it's called!
assert get_array() == ["x"]
assert get_array() == ["x", "x"]
assert get_array() == ["x", "x", "x"]

#   ________________
# _| Comprehensions |__________________________________________________________

# List comprehension
squares1 = [x ** 2 for x in range(1, 6)]
# Generator comprehension
squares2 = (x ** 2 for x in range(1, 6))
# Dictionary comprehension
squares3 = {key: value ** 2 for key, value in zip(range(5), range(1, 6))}

assert all(squares1[i] == next(squares2) == squares3[i] for i in range(5))

# Comprehensions have their own namespace
i = 0
[i for i in range(5)]
assert i == 0

#   ______________________
# _| Array initialisation |____________________________________________________

array1 = [[1, 2, 3] for _ in range(2)]
array2 = [[1, 2, 3]] * 2
assert array1 == array2 == [[1, 2, 3], [1, 2, 3]]

array1[0][0] = 0
assert array1 == [[0, 2, 3], [1, 2, 3]]

array2[0][0] = 0
# Both elements reference the same object!!!
assert array2 == [[0, 2, 3], [0, 2, 3]]

#   ___________________________________
# _| Generators, iterators & iterables |_______________________________________

class SquaresIterator:
    """Iterator class"""

    def __init__(self, lower, upper):
        self.current = lower
        self.upper = upper

    def __iter__(self):
        return self

    def __next__(self):
        if self.current == self.upper:
            raise StopIteration
        self.current += 1
        return self.current ** 2

def squares_generator(lower, upper):
    """Generator function"""
    for current in range(lower, upper):
        yield current ** 2

def even_numbers():
    """Values can be yielded from a generator without iterating over them"""
    yield from range(0, 101, 2)

class SquaresIterable:
    """Iterable with an implicit generator function (NOT an iterator!)"""

    def __init__(self, lower, upper):
        self.lower = lower
        self.upper = upper

    def __iter__(self):
        for current in range(self.lower, self.upper):
            yield current ** 2

assert list(squares_generator(3, 8)) == \
    list(SquaresIterator(3, 8)) == \
    list(SquaresIterable(3, 8))

def bidirectional_generator():
    """Generator that expects data sent into it"""
    num = (yield)
    yield num ** 2

gen = bidirectional_generator()
gen.send(None)  # Initialise the generator
assert gen.send(2) == 4
try:
    gen.send(5)  # Will raise StopIteration
except StopIteration:
    pass

#   __________
# _| Closures |________________________________________________________________

functions = []
for i in range(3):
    functions.append(lambda: i)

# `i` is looked up at the time of calling `f`, not the time of definition!
assert [f() for f in functions] == [2, 2, 2]

# Workaround: default arguments
functions = []
for i in range(3):
    functions.append(lambda j=i: j)

assert [f() for f in functions] == [0, 1, 2]

#   __________________
# _| Context managers |________________________________________________________

class SafeContext:
    """Context manager class"""

    def __enter__(self):
        """Set things up"""
        return "I'm safe"

    def __exit__(self, exception_type, exception_value, traceback):
        """Tear things down"""
        if exception_value:
            raise exception_value

@contextlib.contextmanager
def safe_context():
    """Context manager function"""
    # Set things up
    try:
        yield "I'm safe"
    finally:
        # Tear things down
        pass

with safe_context() as s:
    print(s)

# Contexts don't have their own namespace!
assert s == "I'm safe"

#   _______________________
# _| Decorators & wrappers |___________________________________________________

def prints_at_import(decorated_func):
    """Decorator that isn't a wrapper"""
    print("Decorating")
    return decorated_func

def prints_at_call(wrapped_func):
    """Wrapper without decorator arguments"""
    @functools.wraps(wrapped_func)
    def wrapper(*func_args, **func_kwargs):
        print("Running")
        return wrapped_func(*func_args, **func_kwargs)
    return wrapper

def prints_argument_at_call(argument):
    """Wrapper with decorator arguments"""
    def decorator(wrapped_func):
        @functools.wraps(wrapped_func)
        def wrapper(*func_args, **func_kwargs):
            print(argument)
            return wrapped_func(*func_args, **func_kwargs)
        return wrapper
    return decorator

@prints_at_import
@prints_at_call
@prints_argument_at_call("I'm a wrapper")
def decorated_function():
    print("I'm the function")

#   __________________
# _| Data descriptors |________________________________________________________

class Descriptor:
    """A data descriptor is an object that calls a function when some of its
    attributes are accessed"""

    def __init__(self):
        self._p = None

    def get_p(self):
        return "<{}>".format(self._p)

    def set_p(self, value):
        self._p = value.upper()

    p = property(get_p, set_p)

desc = Descriptor()
desc.p = "foo"
assert desc.p == "<FOO>"

#   ___________________
# _| Inheritance model |_______________________________________________________

class FirstLevel:
    n = 1

class SecondLevelLeft(FirstLevel):
    pass

class SecondLevelRight(FirstLevel):
    n = 2

class ThirdLevel(SecondLevelLeft, SecondLevelRight):
    pass

third_level_obj = ThirdLevel()

# `object` is the base class of everything (old-style classes too)
assert isinstance(third_level_obj, object)

# `__class__` and `type` are the same
assert type(third_level_obj) == third_level_obj.__class__ == ThirdLevel

# Method resolution uses breadth-first search
assert third_level_obj.n == 2

#   ____________
# _| Reflection |______________________________________________________________

def last_but_one(self):
    return self[-2]

# Built-in datatypes cannot be extended
try:
    list.last_but_one = last_but_one
except TypeError:
    pass

# But user-defined types can
class MyList(list):
    pass
MyList.last_but_one = last_but_one

assert MyList([1, 2]).last_but_one() == 1

#   _____________
# _| Metaclasses |_____________________________________________________________

class DefaultClass:
    """`type` is the metaclass of every class"""
    x = 3

# Class definition is evaluated as
DefaultClass = type("DefaultClass", (object,), {"x": 3})

class AutoGetter(type):
    """Metaclass: class of class instances"""

    def __init__(cls, name, bases, dct):
        super().__init__(name, bases, dct)
        for key in dct.keys():
            setattr(cls, "get_" + key, lambda s, k=key: getattr(s, k))

class CustomClass:
    __metaclass__ = AutoGetter
    x = 3

# Class definition is evaluated as
CustomClass = AutoGetter("CustomClass", (object,), {"x": 3})

# `get_x` was auto-generated by the metaclass constructor
assert CustomClass().get_x() == 3

#   ______________
# _| New features |____________________________________________________________

# Python 3.5
if sys.version_info.minor >= 5:
    # Built-in support for coroutines (futures / promises)
    async def slow_operation(m):
        await asyncio.sleep(5)  # A slow operation
        return m ** 2

    op = slow_operation(2)  # Doesn't block
    assert asyncio.run(op) == 4  # Blocks

    # a Task wraps a running coroutine
    task = asyncio.create_task(slow_operation(3))
    assert asyncio.run(task) == 9

    # Type hints

    # Types are not enforced runtime but they can be checked with static
    # analyzers like 'mypy'
    lst: List[str] = []
    lst.append(1)  # This will cause type checking error

    def reversed_numbers(*args: int) -> List[int]:
        return list(reversed(args))

    assert reversed_numbers(1, 2, 3) == [3, 2, 1]

    def print_result(reverser_func: Callable[int, List[int]],
                     args: Sequence[int]) -> None:
        print(reverser_func(*args))

    print_result(reversed_numbers, [1, 2, 3])

# Python 3.6
if sys.version_info.minor >= 6:
    # Literal string interpolation with 'f-strings'
    w = "world"
    assert f"Hello {w}" == "Hello world"

    # Cryptographically secure random numbers
    import secrets
    print(secrets.choice(range(500)))

# Python 3.7
if sys.version_info.minor >= 7:
    # Built-in function for setting a break point
    breakpoint()

# Python 3.8
if sys.version_info.minor >= 8:
    # Assignment expressions with the 'walrus' operator (like in Go)
    lst = [1, 2]
    if (n := len(lst)) >= 2:
        assert n == 2

    nums = range(10)
    while (x := next(nums)) < 5:
        assert x in range(5)

# Python 3.9
if sys.version_info.minor >= 9:
    # Union operator for dictionaries
    dict1 = {"one": 1}
    dict2 = {"two": 2}
    assert dict1 | dict2 == {"one": 1, "two": 2}

# Python 3.10
if sys.version_info.minor >= 10:
    # Union types (like in TypeScript)
    lst: List[int | float] = [1]
    assert isinstance(lst[0], int | float)

    # Match-case (like switch-case)
    n = 20
    match n:
        case 1 | 2:  # OR
            print("A few")
        case x if x > 100:  # Guard
            print("A lot")
        case _:  # Default case
            print("In between")

# Python 3.11
if sys.version_info.minor >= 11:
    # TaskGroup context manager
    async with asyncio.TaskGroup() as tg:
        tg.create_task(asyncio.sleep(1))
        tg.create_task(asyncio.sleep(2))
    # tasks are awaited when the context manager exits

    # Self type
    class Entity:
        @classmethod
        def create(cls) -> Self:
            return cls()

# Python 3.12
if sys.version_info.minor >= 12:
    # Generic functions
    def first[T](elements: Sequence[T]) -> T:
        return elements[0]

    # Generic classes
    class Queue[T]:
        def __init__(self) -> None:
            self.elems: list[T] = []

        def enqueue(self, elem: T) -> None:
            self.elems.append(elem)

    q = Queue[int]()
    q.enqueue(5)

    # Override decorator (like in Java)
    class PriorityQueue[T](Queue):
        @override
        def enqueue(self, elem: T) -> None:
            pass
