/****************************
 * TypeScript Peculiarities *
 ****************************/

// ES-style imports
// For Node.js built-ins do `npm install @types/node`
import { strict as assert } from 'assert';

// Interfaces can be used with objects and classes
export default interface Food {
    taste(): Verdict;
}

// Object types cannot be used with classes
type Verdict = {
    rating: number;
    opinion: any; // Any type (not recommended)
}

// Abstract class
abstract class Soup implements Food {
    // Similar visibility attributes to Java's
    protected static numOfTimesMade: number = 0; // Initial value
    private isGlutenFree: boolean;
    private _countryOfOrigin: string | null // `|`: Union type

    // `?`: optional parameter; may be `undefined`
    constructor(isGlutenFree: boolean, countryOfOrigin?: string) {
        this.isGlutenFree = isGlutenFree;

        // There is no difference between == and ===
        if (countryOfOrigin == undefined) {
          countryOfOrigin = null;
        } else {
          // `!`: ignores that `countryOfOrigin` may be `undefined`;
          // results in compiler error without it
          this._countryOfOrigin = countryOfOrigin!.toUpperCase();
        }

        // Static properties can be accessed via the class and not `this`
        Soup.numOfTimesMade++;
    }

    // Abstract method
    public abstract taste(): Verdict

    // Getter with union return type
    public get countryOfOrigin(): string | null {
        return this._countryOfOrigin;
    }

    // Setter
    public set countryOfOrigin(value: string | null) {
        if (typeof value == 'string') {
            this._countryOfOrigin = value.toUpperCase();
        }
    }
}

export class Gazpacho extends Soup {
    constructor() {
        // Parent constructor call with `super`
        super(true, 'Spain');
    }

    public taste(): Verdict {
        // Compilation error if the keys of the object don't match the ones
        // specified in the interface
        return {
            rating: Gazpacho.numOfTimesMade * 1.5, // Protected property
            opinion: undefined, // `any` type
        }
    }
}

// Generic class; the type parameter must be a subclass of `Food`
class FoodStore<FoodType extends Food> {
    private items: FoodType[] = [];

    // `public` is the default visibility, optional
    add(item: FoodType): void {
        this.items.push(item);
    }
}

const soupStore = new FoodStore<Soup>();
const makeSomeFood = (): Food => new Gazpacho();

// Type assertion: `soupStore.add` expects `Soup` items and not `Food` items
soupStore.add(makeSomeFood() as Soup);
// is the same as
soupStore.add(<Soup>makeSomeFood());

// Getter & setter methods are called at assignment & property access
const gazpacho = new Gazpacho();
gazpacho.countryOfOrigin = 'spain';
assert(gazpacho.countryOfOrigin == 'SPAIN');

// The data type is inferred from the initial value, therefore type annotation
// is optional if the variable gets a value at declaration (like in Go)
const scope = 'Restaurant';
// except for empty arrays and objects where type annotations are mandatory
const shoppingList: Food[] = [];

// Type annotation for an object used as a hash-map
const foodOrder: {[key: number]: Food} = {1: gazpacho};
// is the same as
const foodOrder2: Record<number, Food> = {1: gazpacho};
// The type of the key is enforced when a new element is added,
// however the keys of an object are always strings
assert(Object.keys(foodOrder).every((key) => typeof key == 'string'));

// Solution: generic Map type
const foodPrices = new Map<Food, number>();
foodPrices.set(gazpacho, 10);

// Tuples
let foodOrder3: [number, Food];
// The types are enforced at assignments
foodOrder3 = [1, gazpacho];
// However it doesn't prevent pushing more elements into the array
foodOrder3.push(42);
