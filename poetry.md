# Poetry Notes

- Daily language and literature differ in _intent_ and _intensity_ (Mary Oliver)
- __verse__: the combination of meter, line length and rhyming pattern
- __diction__: the choice of words in a piece of writing

## Meter

- __scansion__: reading a poem in metrical pattern
- __inflection__: alteration of light and heavy stresses in a word
- __foot__: a group of syllables consisting of an emphasised syllable surrounded
  by light syllable(s)
- __iamb__: a light stress followed by a heavy stress `˘ –`
- __trochee__: a heavy stress followed by a light stress `– ˘`
- __dactyl__: a heavy stress followed by two light stresses `– ˘ ˘`
- __anapest__: two light stresses followed by a heavy stress `˘ ˘ –`
- __spondee__: two equal heavy stresses `– –`
- __Pyrrhic foot__: two light stresses `˘ ˘`; only used before a spondee
- __catalexis__: truncation of the final foot of a line
- __caesura__: pause within a line, indicated by a punctuation mark; not counted
  in scansion

## Line

- __trimeter__: a 3-foot line
- __tetrameter__: a 4-foot line
- __pentameter__: a 5-foot line
- __hexameter__: a 6-foot line
- __end-stopped / self-enclosed line__: ends with the grammatical and logical
  completion of a phrase
- __run-on line / enjambment__: a phrase continues over the line ending into the
  next line without a break
- __stanza__: a group of lines separated by an extra amount of space
- __paragraph__: a group of lines whose beginning is indicated by indentation
- __quatrain__: a 4-line metrical stanza

## Rhyme

- __rhyme__: repetition of sounds at the end of line
- __true / perfect rhyme__: the words rhyme using exactly the same vowels
- __slant / imperfect rhyme__: the rhyming words have similar but not identical
  vowels
- __masculine rhyme__: only the final syllables rhyme and they have heavy stress
- __feminine rhyme__: multiple syllables rhyme and the final syllables have
  light stress
- __couplet__: two consecutive rhyming lines in the same meter `a,a`
- __triplet__: three consecutive rhyming lines in the same meter `a,a,a`

## Form

- __blank verse__: iambic pentameter without rhyme (e.g. Shakespeare's plays)
- __free verse__: non-metrical verse
- __stress verse / accentual verse__: metrical verse that has no predominant
  meter
- __syllabic verse__: the number of syllables in each line is strictly repeated
  in every stanza
- __sonnet__: a 14-line-long poem in iambic pentameter with strict rhyming
  patterns
- __Petrarchan sonnet__: _a,b,b,a a,b,b,a | c,d,d c,e,e_  or _c,d,e c,d,e_
  or _c,d,c d,c,d_
- __Shakespearean sonnet__: _a,b,a,b c,d,c,d e,f,e,f g,g_
- __ballad__: a poem with 4-line stanzas; lines 1 and 3 are in tetrameter and 2
  and 4 are in trimeter
- __terza rima__: a poem with 3-line stanzas in iambic pentameter and pattern
  _a,b,a b,c,b c,d,c ..._

## Sound

- __vowels__: _a,e,i,o,u(,w,y)_
- __semivowels__: can be sounded without a vowel: _f,h,j,l,m,n,r,s,v,w,x,y,z_
- __mutes__: block airflow and therefore cannot be sounded without a vowel:
  _b,c,d,g,k,p,q,t_
- __liquids__: fluent semivowels: _l,m,n,r_

## Diction

- __alliteration__: repetition of initial sounds of words in a line
- __assonance__: repetition of vowels within words in a line
- __onomatopoeia__: a word that represents what it defines through its sound
  (e.g. buzz)
- __palindrome__: a word or phrase that is the same when read forwards than read
  backwards (e.g. madam)
- __acrostic__: the first letter of each line spells out a message

## Imagery

- __figure__: a familiar thing linked to something unknown or difficult to
  describe
- __texture__: sensory details that make the reader experience a figure
- __simile__: explicit comparison that uses “like” or “as” to link the subjects
- __metaphor__: implicit comparison
- __personification__: an inanimate object is given personal characteristics
  (e.g. the sky is crying)
- __conceit__: an extended metaphor that continues through the entire poem
- __allusion__: reference to something generally known

## Sensory associations

- taste
- smell
- touch
- hearing
- sight
- a sense of motion

## Rhetoric

- __parallel structure__: keeping the same grammatical structure in successive
  phrases or sentences
- __antithesis__: contrast between parts of a parallel structure, e.g. "Man
  _proposes_, God _disposes_."
- __chiasmus__: A-B-B-A word order, e.g. "_Fair_ is _foul_, and _foul_ is
  _fair_."
- __anaphora__: repeating the same phrase at the beginning of each part, e.g.
  "_We shall_ fight in the fields and in the streets, _we shall_ fight in the
  hills. _We shall_ never surrender."
- __epistrophe__: repeating the same phrase at the end of each part, e.g.
  "Government of _the people_, by _the people_, for _the people_, shall not
  perish from the earth."
- __symploce__: the combination of anaphora and epistrophe for added emphasis,
  e.g. "_We will make America_ proud _again_. _We will make America_ safe
  _again_. And yes, together _we will make America_ great _again_."
- __asyndeton__: conjunctions (_and_, _but_, etc.) are omitted from between
  parts, e.g. "I came, I saw, I conquered."
- __climax__: phrases are arranged in order of increasing importance, e.g.
  "There are three things that will endure: _faith_, _hope_, and _love_."

## Narratives

- __third person__: "He loved her so much."
- __first person__: "I loved her so much."
- __second person__: "You loved her so much."
- __direct address__: "I loved you so much."

## Tools

- Tell a _story_
- Describe an everyday scene, noticing details one wouldn't normally notice
- Add a subtle action that gives the scene tension
- Draw a conclusion from the scene
- Move from life-like to symbolic gradually
- Use everyday objects and places people are familiar with as metaphors
- Talk about your life experience and weaknesses readers can identify with
- Emphasise the time dimension
- Use unfinished sentences or deliberately omit keywords to keep an idea open
- Add elements of surprise, profanity or shock to change the dynamics
- Ask _rhetorical questions_
- Apply _suspension_: reveal key details only in the closing section
- Find your recurring "signature" symbols
