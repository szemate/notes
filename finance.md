# Finance Notes

## Money

- __basis point__: 1 bp = 0.01 %
- __commodity money__: money with intrinsic value, e.g. gold
- __fiat money__: money without intrinsic value, but its value is guaranteed by
  a governing body
- __currency__: money as physical notes and coins representing legal claims
  against the central bank, issued by the central bank, available to everyone
- __deposit__: money as the balance of a digital account representing a legal
  claim against a commercial bank, issued by the commercial bank with 1:1
  exchange value with a currency, available to customers of the commercial bank
- __reserve__: money as the balance of a digital account that a _clearing bank_
  holds at the central bank, representing a legal claim against the central
  bank, issued by the central bank, available to selected banks and financial
  institutions
- __broad money__: instant access money (currencies and bank deposits)
- __narrow money__: claims against the central bank (currencies and reserves)
- __legal tender__: any money that is recognised by law as a means to meet a
  financial obligation
- __clearing bank__: a bank that has an account with the central bank, therefore
  it can borrow central bank reserves and send money to other clearing banks
  without holding accounts at those banks
- __bank rate / base rate__: the interest rate central banks charge clearing
  banks when they take out short-term (overnight) loans of central bank reserves
  to meet temporary shortages of liquidity
- __seigniorage__: the difference between the face value of money and the cost
  to produce it
- __fractional reserve banking__: banks loan out customers' deposits, apart from
  a small fraction left as reserve; the banks that the borrowers transfer the
  loans to when they spend them to do the same, thereby money is in constant
  circulation
- __money creation theory__: commercial banks create money when they issue a
  loan, the money is destroyed when the loan is repaid; similarly governments
  create money when they fund a public investment, the money is destroyed when
  taxes are collected etc.
- __shadow banking system__: entities that provide services similar to
  traditional commercial banks but are outside banking regulations, e.g. hedge
  funds, investment banks, mortgage companies

## Accounting

- __ledger__: a book or database in which accounting transactions are recorded,
  divided into _journals_ of different types of accounts
- __double-entry bookkeeping__: every entry for an account requires an opposite
  entry of the same value for another account; there are two types of entries:
  _debit_ and _credit_, conventionally recorded in two adjacent columns
- __balance sheet__: a summary of the journals of different kinds of accounts
- __debit__: the flow of economic benefit to a destination
- __credit__: the flow of economic benefit from a source
- __assets__: the total value of cash, property, investments, etc. that a
  company owns
- __liabilities__: the total value of loans, wages, etc. that a company owes
  to 3rd-parties (lenders, employees, contractors)
- __owners' equity (capital)__: the total value that a company owes to its
  shareholders
- __company's equity (capital)__: _owners' equity - dividends + retained
  earnings_; the _book worth_ of a company
- __retained earnings__: _revenue - expenses_; profit held for future use
- __DEA-LER rule__: debit increases balances in _dividends_, _expenses_ and
  _assets_ accounts; credit increases balances in _liabilities_, _owners'
  equity_ and _revenue_ accounts
- __accounting equation__: what the company owns equals to what the company
  owes, i.e. _assets = liabilities + company's equity_; or: _dividends +
  expenses + assets = liabilities + owners' equity + revenue_

## Investments

- __time value of money__: the worth of a sum of money is different now than at
  a future date because of inflation and the potential returns of it being
  invested
- __holding period return__: the total return of an investment over its
  lifetime
- __rate of return__: the annual return of an investment, expressed as a
  percentage of the previous year's holdings
- __discounting__: calculating the present value of a future payout, taking
  interest rates into account
- __discount rate__: parameter of the discounting formula that represents the
  expected rate of return of an investment
- __cost-benefit analysis__: evaluating a planned spending by comparing it to
  investing the same amount of money in other ways, by calculating the
  investments' present values
- __asset__: a resource with economic value
- __fungible asset__: whose instances are interchangeable (e.g. money, vote,
  stake)
- __non-fungible asset__: whose instances are unique (e.g. real estate,
  collectibles)
- __instrument__: a tradeable asset

## Securities

- __security__: a financial contract with monetary value, used as an instrument
- __bond__: a security that represents a fraction of a loan to a company
- __stock__: a security that represents a fraction of ownership of a company
- __execution__: signing (committing to) a financial contract
- __clearing__: steps carried out to ensure settlement will happen successfully
- __settlement__: enforcement of a financial contract, the actual exchange of
  assets
- __yield__: the return on an investment, i.e. the interest or dividends
  received from a security
- __tick size__: the smallest possible price increment of one unit of an asset
  quoted for trade in a financial contract
- __point value__: the smallest possible price increment of the total contract
  value, i.e. tick size multiplied by the number of units in the contract
- __face value__: the original price of a security as set by its issuer
- __principal__: the original sum of money borrowed in a loan or put into an
  investment, separate from earnings and interest
- __redemption__: the return of an investor's principal
- __payoff__: the profit or loss from the sale of an instrument after the costs
  have been subtracted
- __maturity__: the date on which an instrument expires, e.g. the day the
  outstanding principal of a bond is repaid
- __novation__: replacing a financial contract with a new one with the constent
  of both parties
- __exit strategy__: a plan by an investor on when and how to sell certain assets
- __collateral__: an asset that a lender accepts as security for a loan; if the
  borrower defaults on the loan payments, the lender can seize the collateral
  and resell it to recoup the losses
- __overcollateralisation__: provision of collateral that is worth more than the
  value of the loan; reduces the risk to investors
- __escrow__: the use of a third party who holds an asset before they are
  transferred from one party to another; the third party holds the funds until
  both parties have fulfilled their contractual requirements
- __investment fund / mutual fund__: an investment scheme that pools money from
  many investors to purchase securities; investors buy a share in a portfolio
  managed by an investment manager in return for a management fee
- __leverage__: borrowing funds or utilising derivatives to undertake an
  investment, which amplifies the potential profit or loss
- __balance sheet leverage__: the use of borrowed capital (stocks, cash) to amplify
  the potential returns of an investment
- __off-balance sheet leverage__: the use of derivatives to amplify the potential
  returns of an investment
- __positive gearing__: when the passive income from a leveraged investment
  exceeds the cost of borrowing
- __negative gearing__: when the passive income from a leveraged investment
  doesn't cover the cost of borrowing; may still be profitable if the asset rises
  in value
- __overleveraging__: using too much leverage for an investment, which almost
  certainly leads to a debt spiral
- __hedge fund__: a high-risk investment fund that invests in derivatives using
  leverage
- __margin account__: an account with a brokerage firm that allows an investor
  to buy securities on leverage, loaned by the broker; there must be enough money
  in the account to cover any losses which may occur
- __initial margin__: the percentage of the purchase price of a security
  (purchased _on margin_, i.e. from a margin account) that an account holder
  must pay for with available cash in the margin account
- __maintenance margin__: a fraction of the initial margin that has to be
  available in the trader's account to keep the trade open; if the trader's
  account balance drops below this threshold then the account receives a _margin
  call_ and has to be replenished to satisfy initial margin requirements
- __variation margin__: the amount of money (as a percentage of the trade
  notional) to be posted in the margin account to return its balance to the
  initial margin level after the maintenance margin has been breached
- __liquidation__: the event that the assets of a margin account holder are sold
  off because they haven't satisfied margin requirements after a margin call
- __cross margining__: transferring money between an investor's margin accounts
  for satisfying margin requirements
- __netting__: aggregating two trading sides' total financial obligations
  towards each other; as a result only one payment is executed between the
  parties
- __compression__: replacing multiple financial contracts with a single
  contract when two trading sides have multiple obligations towards each other

## Exchanges

- __clearing house__: an intermediary that matches buyers with sellers and
  facilitates the clearing process of the trade
- __broker__: an individual or firm that acts as an intermediary between an
  investor and an exchange, buying and selling securities for the investor
- __order / intent__: a request for an exchange to find a counter party for
  buying or selling an asset
- __liquidity__: the measure of how quickly an asset can be sold; depends on the
  amount of orders waiting for a counterparty
- __bid-ask spread__: the difference between the highest price any buyer is
  willing to pay for an asset (_bid_) and the lowest price that any seller is
  willing to accept (_ask_)
- __market order__: an order for buying or selling an asset at the actual market
  price as soon as possible
- __limit order__: an order for buying or selling an asset at the earliest time
  when its price reaches a specified limit; the exchange is then trying to find
  an opposite order that matches or exceeds the bid or ask price
- __slippage__: the difference between the price that is offered when a market
  order is requested and the actual price when the order is executed; the higher
  the liquidity, the smaller the slippage
- __market taker__: an investor with the intent of buying or selling an asset as
  soon as possible through a market order
- __market maker / liquidity provider__: a broker who offers to take either
  side in a bilateral trade by simultaneously quoting bid (buy) and ask (sell)
  prices for assets on an exchange through limit orders, and makes a profit
  from the bid-ask spread
- __request for quotes (RFQ)__: an offer for buying or selling an asset at a
  specified price; the trader publishes an offer, waits for quotes, and decides
  which one to accept
- __central limit order book (CLOB)__: a method for order management whereby the
  exchange collects limit orders from market makers, then they are matched with
  market orders from market takers continuously
- __arbitrage trading__: buying assets on one exchange and selling the same
  assets on another simultaneously, exploiting differences in the price of the
  same asset on the two exchanges
- __high frequency trading__: automated trading by algorithms in high volumes,
  holding portfolios only for seconds, exploiting very short term market
  imbalances
- __wash trading__: buying and selling the same asset simultaneously, in order
  to artificially create high trading volumes for an unpopular asset; disrupts
  the market by feeding misleading information
- __frequent batch auctions / periodic auctions__: a method for order management
  whereby the exchange collects limit orders from market makers and takers that
  are matched periodically at pre-defined intervals; eliminates the unfair
  advantage high frequency traders have on CLOBs
- __PnL limit__: profit and loss limit; the maximum amount of profit and loss a
  trader is willing to take during one business day before they stop trading
- __candlestick chart__: a price chart that displays the highest, lowest,
  opening and closing prices of a security for each day of trading
- __front running__: exploiting inside knowledge during trading; it is
  considered unethical
- __position__: the amount of a security, commodity or currency that someone
  owns; they come in two types: _long positions_ and _short positions_
- __long position__ / __going long__: a security is bought and held,
  anticipating an price increase; to be sold at a future price (i.e. buy low
  then sell high)
- __short position__ / __going short__: a security is borrowed and sold,
  anticipating a price decrease; the borrower will then have to buy at a future
  price in order to return the borrowed security (i.e. sell high then buy low)
- __bullish market__: a market situation in which most traders are going long
- __bearish market__: a market situation in which most traders are going short

## Derivatives

- __hedge__: an investment to reduce the risk of adverse movements in the price
  of an asset; similar to taking out an insurance policy
- __derivative__: a security used for hedging that derives its price from
  fluctuations in the price of an underlying asset; exchanges risk between those
  who want it and those who don't
- __option__: a financial contract that gives the owner the right, but not the
  obligation, to buy an asset (_call option_) or to sell and asset (_put
  option_) at a predetermined future date and price
- __exercising__: activating an option, fulfilling the option contract
- __American option__: the owner of the option can exercise the option any time
  after buying it until the time of expiration
- __European option__: the owner of the option can only exercise it at the time
  of expiration
- __grant / strike price__: the price at which the underlying asset can be
  purchased or sold when exercising an option
- __forward__: a financial contract obligating a buyer to buy an asset or a
  seller to sell an asset at a predetermined future date and price
- __future__: a special forward contract that is traded on an exchange; a
  clearing house guarantees that the transaction will take place at maturity
- __spot price__: the price at which an asset is traded at the moment
- __notional value__: the total value of a position as the number of units in
  the contract multiplied by the spot price of the underlying asset
- __mark to market__: the practice that the value of a futures contract is
  periodically adjusted to follow the current market price; the trading partners
  compensate each other for the gains and losses through payments between their
  margin accounts
- __fair value__: the futures contract value at which traders are neutral
  whether to buy or sell a stock now, or enter a futures contract and lend or
  borrow the equivalent of the current stock price until the expiry of the
  futures contract
- __contango__: a market situation in which the current spot price of an asset
  is lower than the prices of its futures
- __backwardation__: a market situation in which the current spot price of an
  asset is higher than the prices of its futures
- __delta hedging__: trading strategy whereby options and the underlying stocks
  are bought at the same time; this way if the trader loses money on the options,
  they still win some of it back on the stocks
- __swap__: a derivative contract in which the two counterparties exchange cash
  flows that are equal in notional amount but are exposed to different types of
  risks
- __interest rate swap (IRS)__: exchanging the interest payments of a fixed
  interest rate loan for the interest payments of a floating interest rate loan;
  the two parties settle the difference between the interest rate indexes over
  time
- __exchange-traded fund (ETF)__: a security whose value tracks the values of a
  pool of underlying assets, e.g. a stock market index
- __contract for differences (CFD) / non-deliverable forward (NDF)__: a contract
  where two parties take opposite sides of a derivative transaction and settle
  the price difference at maturity; i.e. a bet on price volatility without
  owning the underlying asset
- __collateralised debt obligation (CDO)__: a complex derivative that is backed
  by a bundle of traded debt (bonds, loans etc); as a result investors are
  exposed to a wide range of risks

## Markets

- __capital market__: the market for trading assets
- __spot market__: the market where assets are traded for immediate delivery
- __risk market__: the market for trading derivatives; the underlying assets are
  delivered at future dates
- __Pareto efficiency__: an optimal state of resource allocation in which the
  allocation cannot be changed without making someone worse off
- __perfect competition__: a market structure in which all participants are
  price takers, buyers and sellers have perfect information, and prices are
  determined solely by demand and supply
- __black swan__: an unexpected high-impact event that could even trigger a
  market crash
- __securitisation__: the process through which an issuer creates a tradeable
  instrument from any other asset
- __rentier__: someone who has _passive income_, i.e. income from possession of
  assets that are scarce or artificially made scarce
- __rentier corporation__: a firm that gains most of its income from possession
  of financial assets and intellectual property
- __financialisation__: a pattern of accumulation in which profit making occurs
  mainly through financial transactions rather than production
- __Ponzi scheme__: an investing scam which generates returns for earlier
  investors with money taken from later investors who are promised extraordinary
  returns; it collapses when the flood of new investors dries up

## Corporate Finance

- __voting share / common stock__: a share of ownership that gives the
  shareholder the right to vote on matters of policymaking
- __initial public offering (IPO)__: the stock launch of a company on a public
  exchange, managed by an investment bank that guarantees to buy the stocks if
  there is a lack of public interest
- __profit and loss (PnL) statement__: a report that summarises all incomes and
  costs of a company over a period of time
- __market cap__: the sum of the value of all shares of a publicly traded
  company; the _market worth_ of the company
- __affiliate / associate__: company A is company B's associate if company B
  owns less than 50% of the voting shares of company A
- __subsidiary__: company A is company B's subsidiary if company B owns at least
  50% of the voting shares of company A
- __capitalisation__: the sum of a corporation's stock, long-term debt and
  retained earnings
- __market capitalisation__: a company's outstanding shares multiplied by their
  share price
- __gearing__: the percentage of a company's long-term debt compared to its
  equity capital
- __solvency__: the ability of a company to meet its long-term fixed expenses
- __insolvency__: when a company can no longer meet its financial obligations
  with its lenders as debts become due
- __operating profit__: the profit earned from a firm's normal core business
  operations, without any profit earned from the firm's investments and the
  effects of taxes
- __Return on Assets (ROA) / Return on Invesment (ROI)__: an indicator of how
  profitable a company is relative to its assets; calculated by dividing a
  company's annual earnings by its total assets
- __Return on Equity (ROE)__: measures a corporation's profitability by
  revealing how much profit a company generates with the money shareholders have
  invested; calculated by dividing a company's annual net income with the
  owners' equity
- __price-to-earnings ratio (P/E)__: indicates wheter a corporation's stocks
  are overvalued or undervalued; calculated by dividing the company's stock price
  by its earnings per share
- __forward P/E__: the following quarter's estimated future earnings are used
  for calculating P/E
- __trailing P/E__: the previous 12 months' actual earnings are used for
  calculating P/E
- __environmental, social and governance (ESG) criteria__: a framework for
  rating corporations based on their sustainability record to drive ethical
  investment decisions
- __buyback__: when a corporation buys its own stocks in the open market in
  order to increase the market value of the remaining shares, or to reissue the
  stocks to its employees as stock options
- __leaseback__: when a company sells its assets (machinery, real estate) in
  order to raise cash and then rent the same assets back
- __due diligence__: a financial audit carried out before a major financial
  transaction
- __loan syndication__: when multiple banks fund one loan because it is too
  large and therefore too risky for a single bank to take on
- __private equity fund__: an investment fund that buys large companies that
  are not publicly traded, takes over their management to make them more
  efficient, then sells them
- __venture capital fund__: an investment fund that buys small companies that
  are not publicly traded and are able to realise above average growth using the
  acquired capital; then sells them through an IPO
- __general partner__: a part-owner of a privately owned company
- __hostile takeover__: when an investor takes control of a company by acquiring
  at least 50% of its voting shares against the will of the company's management
- __leveraged buyout__: when an investor purchases a company on leverage i.e.
  with borrowed money, and the assets of the acquired company are used as
  collateral against it, i.e. the acquired company is responsible for paying back
  the debt
- __buy, strip and flip__: when an investor buys a company, raises prices,
  lowers wages, and sells off or closes all of its non-essential parts to cut
  expenses and increase profits, then sells it; typically wrecking the target
  company in the process
- __roll-up merger__: when an investor buys all companies in the same market in a
  geographical area and merges them together, thereby creating a monopoly
- __dividend recap(italisation)__: when a company is forced by its owners to take
  on new debt with the sole purpose of paying it out to the owners as dividend
- __litigation finance__: the process that a third party covers the fees of a
  legal procedure that a company would otherwise be unable to finance, in
  exchange for a portion of the financial compensation if the case is won

## Taxation

- __shell company__: a business created with the sole purpose of holding funds,
  typically in a country where the fund's owners are allowed to be kept secret
- __flow-through entity__: a business created with the sole purpose to pass all
  of its income to its owners so that only the owners' income tax is paid, the
  business doesn't pay any corporate tax
- __profit shifting__: moving the profit that a corporation makes in a country
  where they manufacture products or sell goods and services into a tax haven
- __transfer pricing__: a profit shifting technique whereby a subsidiary of a
  corporation operating in a tax haven sells goods and services at an
  artificially high price to other subsidiaries of the corporation
- __trade misinvoicing__: under-reporting the value of exports that allows
  companies to understate their revenues and thus reduce their income tax
- __carried interest__: profit from private equity or venture capital that is
  taxed as capital gains and not as income due to a tax loophole that exists in
  many countries

## Decentralised Finance (DeFi)

- __central bank digital currency (CDBC) / e-money__: money as digital tokens
  representing legal claims against the central bank, issued by the central
  bank, available to everyone
- __cryptocurrency__: money as digital tokens trusted by users of a blockchain
  network, not reliant on any central authority
- __asset-backed / security token__: a cryptocurrency backed by an external
  asset (e.g. USD) as collateral
- __stablecoin__: a cryptocurrency whose value is pegged to a non-volatile asset
  (e.g. USD, gold)
- __fiat-backed stablecoin__: for each unit of the cryptocurrency one unit of
  the fiat currency is held in a reserve as collateral
- __crypto-backed stablecoin__: for each unit of the cryptocurrency one unit of
  another cryptocurrency is held as collateral
- __algorithmic stablecoin__: the value of the coin is maintained through the
  algorithmic reduction and increase of the coin's supply; the process is called
  _algorithmic stabilisation_
- __centralised exchange (CEX)__: an app for exchanging fiat currency (e.g.
  USD) to cryptocurrency; operated by a financial company and adheres to
  financial regulations
- __decentralised exchange (DEX)__: a Đapp for exchange between
  cryptocurrencies anonymously and without intermediaries being involved
- __custodial exchange__: that holds traders' funds (including their wallets'
  private keys) and is responsible for protecting the funds
- __non-custodial exchange__: that doesn't hold traders' funds and keys
- __staking__: locking up cryptocurrency in a smart contract to help support the
  security and operation of the blockchain network; rewarded with a share of the
  transaction fees
- __liquid staking__: staking while users can still access a derivative of their
  locked up funds; they stake their crypto assets via an escrow service that in
  turn provides derivatives of their assets that they can invest
- __synthetic asset__: a virtual asset that tracks the price of a real world
  asset, without actual ownership of the asset; traders buy and sell it in order
  to bet on market volatility
- __perpetual contract__: a derivative similar to a futures contract but
  without an expiration date and its index price follows the market price of
  an underlying asset
- __automated market maker (AMM)__: a type of exchange that automatically
  creates buyers for sell orders and sellers for buy orders, and applies some
  mechanism that prevents a large imbalance between the number of buyers and
  sellers
- __liquidity pool__: funds deposited by _liquidity providers_ that the AMM uses
  to fulfil orders; providers earn trading fees from the trades that happen in
  their pool, proportional to their share of the total liquidity
- __impermanent loss__: the situation that the value of the locked assets
  decreases in a liquidity pool compared to when they were deposited
- __yield farming / liquidity mining__: generating passive income from crypto by
  locking tokens in liquidity pools and earning trading fees
- __flash loan__: borrowing crypto, using the borrowed money for arbitrage
  trading, and repaying the loan; all within the same transaction
