# Music Theory Notes

<!-- toc -->

- [Music](#music)
- [Sheet](#sheet)
- [Rhythm](#rhythm)
- [Performance](#performance)
- [Intervals](#intervals)
- [Scales](#scales)
- [Keys](#keys)
- [Chords](#chords)
- [Harmonic functions](#harmonic-functions)
- [Chord scales](#chord-scales)
- [Sight reading](#sight-reading)
- [Structure](#structure)
- [Harmony](#harmony)
- [Melody](#melody)
- [Composition](#composition)
- [Classical music](#classical-music)
- [Jazz](#jazz)
- [Vocals](#vocals)
- [Guitar](#guitar)
- [Piano](#piano)
- [Percussion](#percussion)
- [String instruments](#string-instruments)
- [Wind instruments](#wind-instruments)

<!-- tocstop -->

## Music

- __melody__: an arrangement of successive notes
- __motion__: the way the pitch of successive notes changes
- __timbre / tone colour / tone quality__: the way a single note sounds
- __chord__: a combination of at least 3 notes played together
- __harmony__: an arrangement of successive chords
- __range__: the distance between the highest and the lowest pitch
- __voice__: a melodic part in a specific tonal range
- __texture__: the relationship between voices played simultaneously
- __counterpoint__: the study of harmonic relationships between melody lines
- __just / pure intonation__: all intervals are expressed as whole number ratios
- __justly tuned diatonic scale__: an octave is divided into 7 notes where the
  relative frequencies follow just intonation
- __12-tone equal temperament__: an octave is divided into 12 equally spaced
  intervals; justly tuned diatonic scales starting from any of the 11 tones can
  be approximated using a combination of single intervals (semitone) and double
  intervals (tone)
- __compound interval__: interval bigger than an octave
- __pitch class__: the set of all notes an octave apart (e.g. all C notes)
- __microtone__: an interval smaller than a semitone in the 12-tone equal
  temperament
- __xenharmonic music__: music that is not based on a 12-tone tuning system
- __sight-reading__: performing written music unseen before
- __aural skills__: the ability to identify the notes of a musical piece unheard
  before
- __rhythmic entrainment__: humans' innate feature of syncing up with a steady
  rhythm from the environment

### Texture types

- __monophonic / unison music__: that consists of a single melody line
- __homophonic / chordal music__: that consists of block chords; all voices move
  in parallel motion
- __polyphonic / contrapuntal music__: that consists of multiple melody lines

## Sheet

- __note / tone__ = pitch + duration
- __score__ = tempo + rhythm + melody + harmony + dynamics
- __stave / staff__: 5 parallel lines for visualising notes
- __ledger line__: extension line above or below the staff
- __brace / {__: notation symbol that groups staves representing multiple
  ranges of an instrument
- __grand stave__: two staves joined by a brace, to be played at once
- __clef__: indication of which notes the lines of the stave represent
- __treble clef / G-clef__: G is on the 2nd line of the stave
- __bass clef / F-clef__: F is on the 4th line of the stave
- __C-clefs__: C is on the 3th line of the stave
- __accidental__: a pitch modifier (a sharp or a flat or a natural)
- __sharp / ♯__: raises the note by a semitone until the end of bar
- __flat / ♭__: lowers the note by a semitone until the end of bar
- __natural / ♮__: cancels a previous sharp or flat until the end of bar
- __8va__: indication that a sequence of notes shall be played an octave higher
  or lower (to avoid using too many ledger lines)

## Rhythm

- __beat__: the pulse of a piece music; can usually be felt intuitively
- __meter__: denotes the rhythmic shape of music, i.e. indicates the length of a
  beat, the placement of strong and weak beats, and the counting pattern
- __time signature__: a fractional number that indicates the meter in the sheet
- __simple time__: a meter in which the length of a beat is a simple note (4th
  note or 8th note); e.g. in 3/4 time a beat is 1/4 note long, every 3rd beat
  is strong, counted as _1 2 3_
- __compound time__: a meter in which the length of a beat is a dotted note
  (dotted 4th note = 3/8 or dotted 8th note = 3/16); e.g. in 6/8 time a beat is
  3/8 note long, every other beat is strong, counted as _1 & a 2 & a_
- __irregular time__: a meter that cannot be expressed with equal-length beats;
  e.g. in 5/8 time the pattern is a 3/8-note-long beat followed by a
  2/8-note-long beat, counted as _1 & a 2 &_ or _1 & 2 & a_
- __common time (C)__: 4/4 time
- __bar / measure__: the smallest level of musical organisation; a group of
  notes with a fixed length, determined by the time signature
- __rest__: an interval of silence with a specified duration
- __pause / fermata__: an out of time rest; its length is chosen by the player
- __beam__: line that connects adjacent notes in the sheet to simplify notation
- __tie__: indication of extending the duration of a note by the duration of the
  tied note
- __dot (.)__: indication of extending the duration of a note by half of its
  original duration
- __double dot (..)__: indication of extending the duration of a note by
  three-quarters of its original duration
- __tuplet__: division of a beat into a number of subdivisions that is not
  permitted by the meter
- __triplet__: division of a beat into 3 in simple time (i.e. switching
  temporarily into compound time)
- __duplet__: division of a beat into 2 in compound time (i.e. switching
  temporarily into simple time)
- __downbeat / on-beat__: the odd beats of a bar
- __upbeat / off-beat__: the even beats of a bar
- __backbeat__: accenting the upbeats (beats 2 and 4 in common time)
- __syncopation__: a rhythm that doesn't follow the beat
- __anacrusis / upbeat start__: a piece of music starts with a note or a short
  sequence of notes before the first beat of the first bar
- __pickup note__: a single note anacrusis
- __tenuto (_)__: holding the sound for the entire duration of the note
- __staccato (.)__: cutting the sound short and keeping a rest for the remaining
  duration of the note
- __swing__: two consecutive 8th notes are played with a triplet feel, i.e.
  roughly as the 1st and 3rd notes of a triplet

### Relative durations

- 1 = whole note = __semibreve__
- 3/4 = dotted 1/2 note
- 1/2 note = __minim__ [too]
- 3/8 = dotted 1/4 note [tum]
- 1/4 note = __crotchet__ [ta]
- 1/8 note = __quaver__ [ti]
- 3/16 = dotted 1/8 note [tim]
- 1/16 note = __semiquaver__ [ti-ri]
- 1/32 note = __demisemiquaver__
- 1/64 note = __hemidemisemiquaver__
- 1/3 note = __triplet__ [tri-o-la]

### Counting patterns

- 1/4 subdivision: _1 2 3 4_
- 1/8 subdivision: _1 & 2 & 3 & 4 &_
- 1/16 subdivision: _1 e & a 2 e & a 3 e & a 4 e & a_

### Polyrhythm

- __polyrhythm__: multiple rhythms with different subdivisions played
  simultaneously
- __X:N polyrhythm / cross-rhythm__: two rhythms with different subdivisions
  played simultaneously, one subdivides the bar to N beats (primary pulse) and
  the other to X beats (secondary pulse)
- __hemiola__: a 3:2 (or 2:3) polyrhythm

## Performance

### Tempo

- __lento, largo__: very slow (~40 bpm)
- __adagio__: slow (~60 bpm)
- __andante__: at a walking pace (~90 bpm)
- __moderato__: moderately fast (~110 bpm)
- __allegro, vivace__: fast (~140 bpm)
- __presto__: very fast (~180 bpm)
- __a piacere__: the tempo is up to the performer
- __tempo giusto__: strict tempo
- __tempo rubato__: free tempo; the performer is allowed to freely “stretch” or
  “squeeze” the tempo of a phrase
- __accelerado__: gradual increase in tempo
- __ritardando__: gradual decrease in tempo

### Dynamics

- __accent (>)__: emphasis on one note
- __marcato (∧)__: accented staccato note (emphasised and short)
- __piano__: soft
- __forte__: strong
- __mezzo~__: moderately
- __~issimo__: very much
- __subito__: immediate change in loudness
- __subito forzando__ / __sforzando__: immediate forceful increase in loudness
- __crescendo__: gradual increase in loudness
- __decrescendo__ / __diminuendo__: gradual decrease in loudness

### Ornaments

- __legato__: connecting the subsequent notes smoothly, without separation
- __slur__: symbol in the sheet that groups notes to be played legato
- __glissando__: sliding from a pitch to another through a succession of
  discreet notes (e.g. on the guitar, piano)
- __portamento__: sliding from a pitch to another continuously (e.g. on the
  violin, trombone)
- __grace note__: a single note ornament
- __acciaccatura / crush note__: a grace note played fast, doesn't take away
  from the decorated note's duration
- __appoggiatura__: a grace note played slow, usually halves the decorated
  note's duration
- __trill__: quickly alternating between the decorated note and a note above it
- __mordent__: playing a note, quickly playing a note above it, and returning to
  the decorated note
- __vibrato__: varying the pitch of an extended note rapidly, which results in a
  pulsating effect

## Intervals

- _0 semitone_: perfect 1st (P1) = diminished 2nd = prime = unison
- _1 semitone_: minor 2nd (m2) = augmented 1st = semitone (s)
- _2 semitones_: major 2nd (M2) = diminished 3rd = tone (T)
- _3 semitones_: minor 3rd (m3) = augmented 2nd
- _4 semitones_: major 3rd (M3) = diminished 4th
- _5 semitones_: perfect 4th (P4) = augmented 3rd
- _6 semitones_: diminished 5th = augmented 4th = tritone (TT)
- _7 semitones_: perfect 5th (P5) = diminished 6th
- _8 semitones_: minor 6th (m6) = augmented 5th
- _9 semitones_: major 6th (M6) = diminished 7th
- _10 semitones_: minor 7th (m7) = augmented 6th
- _11 semitones_: major 7th (M7) = diminished 8th
- _12 semitones_: perfect 8th (P8) = augmented 7th = octave

### Harmonic classification of intervals

- __unison__
- __perfect consonance__: octave, perfect 5th, perfect 4th
- __imperfect consonance__: major and minor 3rd and 6th
- __dissonance__: all other intervals

### Inverted intervals

- interval + inverted interval = 9
- 2nd → 7th
- 3rd → 6th
- 4th → 5th
- 5th → 4th
- 6th → 3rd
- 7th → 2nd
- minor → major
- major → minor
- augmented → diminished
- diminished → augmented
- perfect → perfect

### Overtone series

- fundamental: the frequency of a pure tone
- partial: a component of a complex tone above the fundamental
- harmonic: a partial whose frequency is an integer multiple of the fundamental
- 2:1 ratio: P8 interval
- 3:2 ratio: P5 interval
- 4:3 ratio: P4 interval
- 5:4 ratio: M3 interval
- 6:5 ratio: m3 interval

### Tonal degrees

#### Diatonic

- _P1_: do / tonic
- _M2_: re / supertonic
- _M3_: mi / mediant
- _P4_: fa / subdominant
- _P5_: sol / dominant
- _M6_: la / submediant
- _M7_: ti / subtonic = leading tone

#### Chromatic

- _P1_: do
- _m2_: di / ra
- _M2_: re
- _m3_: ri / me
- _M3_: mi
- _P4_: fa
- _TT_: fi / se
- _P5_: sol
- _m6_: si / le
- _M6_: la
- _m7_: li / te
- _M7_: ti

## Scales

- __pentatonic / heptatonic / octatonic scale__: comprises of 5 / 7 / 8 notes
  within an octave
- __diatonic scale__: a heptatonic scale with a combination of 5 tone and 2
  semitone intervals; each letter should be represented in the scale (e.g. F is
  called E# in F# major)
- __chromatic scale__: comprises of all 11 diatonic semitone steps
- __relative scales__: that include the same intervals but start from different
  roots
- __parallel scales__: that start from the same root note but include different
  intervals
- __major pentatonic scale__: T T m3 T m3 intervals
- __major diatonic scale__: T T s T T T s intervals

### Pentatonic scales

- __major pentatonic scale__: do re mi sol la do
- __minor pentatonic scale__: starts on the 5th of the major scale / do me fa sol te do
- __dominant pentatonic scale__: major pentatonic with minor 7th /
  do re mi sol te do
- __minor 6th pentatonic scale__: minor pentatonic with major 6th /
  do me fa sol la do
- __minor blues scale__: minor pentatonic + flat 5th _blue note_
- __major blues scale__: major pentatonic + flat 3rd _blue note_

### Modes of the major scale

#### Major

- __Ionian__: do re mi fa sol la ti do
- __Lydian__: Ionian with sharp 4 / do re mi fi sol la ti do

#### Dominant

- __Mixolydian__: Ionian with flat 7 / do re mi fa sol la te do

#### Minor

- __Aeolian__: Ionian with flat 3, 6, 7 / do re me fa sol le te do
- __Dorian__:  Ionian with flat 3, 7 / Aeolian with sharp 6 /
  do re me fa sol la te do
- __Phrygian__: Ionian with flat 2, 3, 6, 7 / Aeolian with flat 2 /
  do ra me fa sol le te do

#### Half-diminished

- __Locrian__: Ionian with flat 2, 3, 5, 6, 7 / Aeolian with flat 2, 5 /
  do ra me fa se le te do

### Minor scales

- __natural minor__: the Aeolian mode
- __harmonic minor__: Ionian with flat 3, 6 / Aeolian with sharp 7 /
  do re me fa sol le ti do
- __ascending melodic minor__: Ionian with flat 3 / Aeolian with sharp 6, 7 /
  do re me fa sol la ti do
- __descending melodic minor__: the natural minor

### Jazz scales

#### Dominant

- __Lydian b7__: Ionian with sharp 4, flat 7 / Lydian with flat 7 /
  do re mi fi sol la te do

#### Minor

- __Ionian b3__: the ascending melodic minor played both ascending and
  descending

#### Half-diminished

- __altered / super Locrian / Locrian b4__: Ionian with flat 2, 3, 4, 5, 6, 7 /
  Locrian with flat 4 / do ra me fe se le te do
- __half-diminished scale / Locrian ♮2__: 6th mode of the melodic minor / Ionian
  with flat 3, 5, 6, 7 / Locrian with natural 2 / do re me fa se le te do

### Octatonic scales

#### Diminished

- __half-whole scale__: 1 b2 b3 b4 b5 5 6 b7 / do ra me fe se so la te do
- __whole-half scale__: 1 2 b3 4 b5 b6 6 7 / do re me fa se le la ti do

### Hexatonic scales

#### Augmented

- __whole tone scale__: 1 2 3 #4 #5 #6 / do re mi fi si li do

## Keys

- __tonic = root = base = keynote__: the note a scale is built on
- __signature__: a combination of sharps or flats, valid until the end of sheet;
  if there are sharps in a signature, there cannot be flats; if there are flats,
  there cannot be sharps
- __key__: a root note and a signature that define a scale that a piece of music
  is composed in
- __relative major / minor__: every signature defines two keys, a major and a
  minor key whose root notes are a minor third apart
- __tonal centre__: the root note of a key; enharmonic with all chords in the
  given key
- __accidental__: a note that is not in the key of the most recent signature
- __circle of 5ths__: a 5th step in tonic adds one sharp note, a 4th step adds
  one flat note into the signature
- __closely related keys__: for every key there are 5 keys that differ in zero
  or one sharp or flat: the relative major or minor and the keys defined by the
  adjacent signatures in the circle of 5ths

### Signature identification

- _pattern of sharps in a signature_: F C G D A E B, clockwise movement in the
  circle of 5ths (mnemonic: Father Christmas Gets Dad An Electric Blanket)
- _pattern of flats in a signature_: B E A D G C F, anticlockwise movement in
  the circle of 5ths (mnemonic: Blanket Explodes And Dad Gets Cold Feet)
- _sharp signatures_: in major keys the last sharp of the signature is the
  leading note of the root
- _flat signatures_: in major keys the second-last flat of the signature is the
  root note itself; 1b is F major
- 5b has the same modified notes as 7#
- 7b has the same modified notes as 5#
- 6b has the same modified notes as 6#

### Signature calculation

#### Signatures to remember

- _F major_: 1b
- _C major_: no modified note
- _G major_: 1#
- _D major_: 2#
- _A major_: 3#
- _E major_: 4#
- _B major_: 5#

#### Calculations

- _numeric value of a sharp note in a signature_: +1
- _numeric value of a flat note in a signature_: -1
- _convert a key to its parallel sharp key_: add 7 to its numeric value
- _convert a key to its parallel flat key_: subtract 7 from its numeric value
- _convert a major key to its parallel minor_: subtract 3 from its numeric value
- _convert a minor key to its parallel major_: add 3 to its numeric value
- _modified notes in a signature_: all the unmodified notes in its parallel
  sharp or flat signature

### Key identification

#### In heard music

1. listen to the tune
2. hum the tonic; in tonal music it can be found by intuition
3. build major and minor scales on that note and see which one fits the tune

#### In written music

1. identify the signature; it defines 2 keys (a major & its relative minor)
2. look for the raised 7th; it implies harmonic minor scale
3. the bass note of the opening chord is usually the tonic
4. find authentic cadences and see if they are major or minor

### Interval identification

1. count the diatonic steps between the notes
2. build the major (Ionian) scale on the lower note
3. see whether the higher note fits the scale tone or it is sharp or flat

## Chords

### Triads

- __triad__: chord that comprises of two 3rd intervals
- __major triad__: P1 M3 P5 / do mi sol
- __minor triad [-]__: P1 m3 P5 / do me sol
- __augmented triad [+]__: P1 M3 aug5 (major with aug5) / do mi si
- __diminished triad [o]__: P1 m3 dim5 (minor with dim5) / do me se

### Suspended chords

- __suspended 2nd [sus2]__: P1 M2 P5 / do re sol
- __suspended 4th [sus4]__: P1 P4 P5 / do fa sol

### 6th chords

- __major 6th [6]__: major triad + M6 / do mi sol la
- __minor 6th [-6]__: minor triad + M6 / do me sol la

### 7th chords

- __major 7th [Δ7]__: major triad + M7 / do mi sol ti
- __dominant 7th [7]__: major triad + m7 / do mi sol te
- __minor 7th [-7]__: minor triad + m7 / do me sol te
- __minor-major 7th [Δ̲7]__: minor triad + M7 / do me sol ti
- __augmented 7th [+7]__: augmented triad + m7 / do mi si te
- __augmented-major 7th [+Δ7]__: augmented triad + M7 / do mi si ti
- __half-diminished 7th [ø7]__: diminished triad + m7 / do me se te
- __diminished 7th [o7]__: diminished triad + dim7 / do me se la

### Extended chords

- __tertiary chord__: comprises of intervals of 3rds
- __9th__: 7th chord + M9 / … re
- __11th__: 9th chord + P11 / … re fa
- __13th__: 11th chord + P13 / … re fa la

### Non-diatonic chords

- __quartal chord__: comprises of P4 itervals / P1 P4 m7 / do fa te
- __quintal chord__: comprises of P5 itervals / P1 P5 M9 / do sol re
- __pentatonic chord__: comprises of the 1st, 3rd and 5th degrees of a mode of
  the pentatonic scale
- __bitonal chord__: 2 chords from 2 different keys stacked on top of each
  other
- __tone cluster__: a chord that contains at least 3 adjacent notes of a scale,
  resulting in a dissonant sound
- __tetrachord__: a chord that comprises of 4 adjacent notes of a scale; any
  diatonic scale can be built up from 2 tetrachords starting on I and V

### Chord inversions

- __chord inversion__: raising some of the notes of a chord by an octave
- __figured bass notation__: denoting chords in the sheet by the root note and
  numbers representing intervals from the root in descending order

#### Triad inversions

- _triad root position_ [none or 5/3]: 1 3 5 degrees (3rd, 5th from the root)
- _triad 1st inversion_ [6 or 6/3]: 3 5 1 degrees (3rd, 6th from the new root)
- _triad 2nd inversion_ [6/4]: 5 1 3 degrees (4th, 6th from the new root)
- _augmented chord_: its inversions are root position augmented chords with
  major 3rd root movement
- _diminished chord_: its inversions are root position diminished chords with
  minor 3rd root movement

#### 7th inversions

- _7th chord root position_ [7]: 1 3 5 7 degrees (3rd, 5th, 7th from the root)
- _7th chord 1st inversion_ [6/5]: 3 5 7 1 degrees (3rd, 5th, 6th from the new
  root)
- _7th chord 2nd inversion_ [4/3]: 5 7 1 3 degrees (3rd, 4th, 6th from the new
  root)
- _7th chord 3rd inversion_ [2 or 4/2]: 7 1 3 5 degrees (2nd, 4th, 6th from the
  new root)

#### Enharmonic chords

- a minor 7th [6/5] chord is enharmonic to the root position major 6th chord a
  minor 3rd above

### Memorising chords

- the root and the 5th have the same accidental in all major & minor chords
  except for B and Bb
- the root and the 3rd have the same accidental only in C, F, Gb, G major
  chords

## Harmonic functions

### Diatonic functions

- _1_: tonic
- _2_: supertonic
- _3_: mediant
- _4_: subdominant
- _5_: dominant
- _6_: submediant
- _7_: subtonic = leading tone

### Chord functions

- __tonic__: 1 degree; stable
- __tonic prolongation__: 3 & 6 degrees; transitional chord that tends to
  connect the tonic and the pre-dominant
- __pre-dominant__: 2 & 4 degrees; transitional chord that tends to progress to
  the dominant
- __dominant__: 5 & 7 degrees; tension chord that needs to be resolved to the
  tonic
- the dominant 7th chord has tritone interval between the M3 and m7, i.e. it has
  the greatest tension

### Roman numeral notation

- _upper case_: the harmony built on the scale degree has major 3rd
- _lower case_: the harmony built on the scale degree has minor 3rd

### Chords of major & minor scales

#### Triads

- _major scale_: I maj, ii min, iii min, IV maj, V maj, vi min, viio dim
- _natural minor scale_: i min, iio dim, III maj, iv min, v min, VI maj, VII maj
- _harmonic minor scale_: i min, iio dim, III+ aug, iv min, V maj, VI maj,
  viio dim
- _melodic minor scale_: i min, ii min, III+ aug, IV maj, V maj, vio dim,
  viio dim

#### 7th chords

- _major scale_: IΔ maj7, ii min7, iii min7, IVΔ maj7, V dom7, vi min7, viiø
  half-dim7
- _natural minor scale_: i min7, iiø half-dim7, IIIΔ maj7, iv min7, v min7, VIΔ
  maj7, VII dom7
- _harmonic minor scale_: iΔ̲ min-maj7, iiø half-dim7, III+Δ aug-maj7, iv min7,
  V dom7, VIΔ maj7, viio dim7
- _melodic minor scale_: iΔ̲ min-maj7, ii min7, III+Δ aug-maj7, IV dom7, V dom7,
  viø half-dim7, viiø half-dim7

## Chord scales

- __substructure__: notes taken from a scale or chord that together define
  another scale or chord
- __lower structure__: the 1 3 5 7 degrees of an extended chord
- __upper structure__: the 9 11 13 degrees of an extended chord
- __chord scale__: a diatonic scale can be thought of as a rearranged 13th chord
  where the 1 3 5 7 degrees of the scale define the lower structure and the 2 4
  6 degrees of the scale define the upper structure (equivalent to the 9 11 13
  degrees of the chord); every chord implies a set of scales that fit the lower
  structure and define the upper structure of the chord
- __baseline scale__: the chord scale that extends a lower structure chord to
  its unaltered 13th chord
- __superimposed chord__: a triad taken from a diatonic scale that together with
  the lower structure chord forms an extended chord; e.g. in the C Ionian scale
  the lower structure chord is C maj7; the triad starting on the 5th degree is
  G maj; C maj7 + G maj = C maj9
- __superimposed pentatonic__: a pentatonic scale starting from one of the chord
  degrees, used as a chord scale; e.g. the minor pentatonic from the 5th of a
  min7 chord extends the min7 to min11
- __playing outside the chord__: using a chord scale over a chord that fits a
  substitution of the chord; e.g. B Mixolydian scale over an F dom7 chord
  because B dom7 is the tritone substitution of F dom7

### Selecting a scale for a chord

1. the chord symbol makes it clear, i.e. only one scale fits all chord notes
2. match the notes of an already written melody in the song
3. match the notes of the preceding or succeeding chord, emphasising the
   harmonic motion

### Baseline scales

- __Ionian__: maj13 chord
- __Mixolydian__: dom13 chord
- __Dorian__: min13 chord
- __half-diminished scale / Locrian ♮2__: half-dim13 chord

### "Tension" scales

- __Lydian__: maj13 chord with #11
- __Aeolian__: min13 chord with b13
- __Phrygian__: min13 chord with b9, b13
- __Lydian b7__: dom13 chord with #11
- __Ionian b3__: min-maj13 chord
- __Locrian__: half-dim13 chord with b9
- __altered / super Locrian / Locrian b4__: half-dim13 chord with b9, b11

### Avoid notes

- Melody notes a semitone above any chord tone sound very dissonant and should
  be avoided or used only as passing notes
- __Ionian__: the 4th (over maj7)
- __Dorian__: no avoid note (over min7)
- __Phrygian__: the 2nd & the 6th (over min7)
- __Lydian__: no avoid note (over maj7)
- __Mixolydian__: the 4th (over dom7)
- __Aeolian__: the 6th (over min7)
- __Locrian__: the 2nd (over half-dim7)

## Sight reading

### Solfège

- __fixed do system__: do is always C (used in Latin speaking countries)
- __movable do system__: do is the tonic
- __la based minor__: do is the tonic of the relative major (Ionian) scale (used
  in Central & Eastern Europe)
- __do based minor__: do is the tonic of every scale (used in English speaking
  countries & Northern Europe)
- __Kodály method__: movable do with la based minor and rhythm syllables

### Notes

- _G clef lines_: E G B D F (mnemonic: *E*very *G*ood *B*oy *D*eserves *F*udge)
- _G clef spaces_: F A C E
- _F clef lines_: G B D F A (mnemonic: *G*rizzly *B*ears *D*on't *F*ear
  *A*nything)
- _F clef spaces_: A C E G (mnemonic: *A*ll *C*ows *E*at *G*rass)

### Reference notes

- __reference note__: a note in a stave that shall be immediately recognised;
  other notes shall be read by recognising the interval from the closest
  reference note
- the G and F clefs show the places of *G* and *F*
- the top line of G clef is *F*, the bottom line of F clef is *G*
- the 1st lower ledger line of G clef and the 1st upper ledger line of F clef
  are the *middle C* (the same note)
- the 2nd space from the top of G clef and the 2nd space from the bottom of F
  clef are *C*
- the 2nd upper ledger line of G clef and the 2nd lower ledger line of F clef
  are *C*

### Intervals

- _2nd_: adjacent line and space
- _3rd_: 2 adjacent lines or 2 adjacent spaces
- _4th_: one note is on a line, the other one is on a space, and there is 1 line
  between them
- _5th_: both notes are on a line and there is 1 line between them; or both are
  on a space and there is 1 space between them
- _6th_: one note is on a line, the other one is on a space, and there are 2
  lines between them
- _7th_: both notes are on a line and there are 2 lines between them; or both
  are on a space and there are 2 spaces between them
- _octave_: one note is on a line, the other one is on a space, and there are 3
  lines between them

### Middle C

- the 4th C on a regular piano keyboard
- the 3rd C on a 49-key keyboard
- 5th string 3rd fret on the guitar
- 1st string 5th fret on the bass

### Preparation

- identify the range (the lowest and the highest notes) to determine the
  position (on string instruments)
- identify the key and play the major scale to know which notes are likely to be
  used in the piece
- identify the rhythmic divisions (the positions of accented notes)
- __clumping__: looking at multiple bars and identifying any leap intervals,
  scales and chords used
- __tiering__: identifying voices in polyphonic music in order that the
  intervals can be followed in each voice

## Structure

- __motif / motive__: a short musical idea
- __phrase__: a group of motives
- __sentence__: a phrase that is closed with a cadence
- __cadence__: a chord progression that closes a sentence
- __section__: a group of phrases
- __double bar__: symbol that indicates the borders of sections in the sheet
- __turnaround__: a cadence that connects a section with its repetition
- __harmonically closed section__: that ends on the tonic chord
- __harmonically open section__: that ends on any chord other than the tonic
- __period__: a couple of connected sentences, with the 2nd one having a more
  conclusive cadence
- __antecedent phrase / question__: the 1st phrase of a period, harmonically
  open
- __consequent phrase / answer__: the 2nd phrase of a period, harmonically
  closed
- __elision__: overlapping 2 phrases; the last bar of the 1st phrase is the
  first bar of the 2nd phrase
- __movement / song__: a group of sections that form a complete composition
- __theme / subject__: a phrase that is the main musical idea of a section and
  gets expanded through the section
- __transition / bridge__: a phrase that links themes
- __closing__: a phrase that is harmonically stable and implies a sense of
  completeness
- __coda / outro__: a section that consists of one prolonged cadence that brings
  a movement to an end

### Repetition

- __da capo__: repeat from the beginning
- __dal segno__: repeat from the sign
- __al coda__: repeat until the coda section
- __al fine__: repeat, then play until the end

### Cadences

- __perfect authentic cadence__: V[6/4] - I[6], dominant to tonic where the
  highest voice is the leading tone resolving to the tonic
- __imperfect authentic cadence__: V - I[6/4] or V[6] - I, dominant to tonic
  where the leading tone resolves to tonic in lower voices
- __plagal / amen cadence__: IV - I or ii - I, subdominant to tonic
- __half / imperfect cadence__: ends on the dominant, typically I - V or IV - V
- __deceptive / interrupted cadence__: ends on any other degree, typically V -
  IV or V - ii or V - vi
- __cadential six-four__: I[6/4] - I or I[6/4] - V - I, tonic inversion to
  tonic

## Harmony

### Chord progression

- __functional harmony__: each chord in a progression has a harmonic function
  relative to the root of the key and typically follows a subdominant
  (transition) - dominant (tension) - tonic (resolution) pattern (IV-V-I or
  ii-V-I)
- __non-functional harmony / pandiatonicism__: each chord in a progression
  exists in isolation, doesn't take up any harmonic function, and may not offer
  any resolution
- __harmonic rhythm__: the pattern of how harmonic functions follow each other
  in a progression (e.g. 2 bars of tonic, 1 bar of subdominant, 1 bar of
  dominant)
- __modulation__: changing the key in the middle of a tune
- __passing chord__: an out-of-key chord that connects two chords of a
  progression
- __substitution__: replacing a chord in a progression
- __extension__: substituting a chord with its extended variant; e.g. maj9
  instead of maj7
- __diatonic substitution__: substituting a chord with another one that has the
  same harmonic function in the same key; e.g. the I with the iii
- __borrowed chord / modal interchange__: substituting a chord with another one
  on the same degree in a parallel mode; e.g. the ii with the bIImaj7 from the
  parallel Phrygian mode (_Neapolitan substitution_)
- __tritone / flat five substitution__: substituting a dominant chord with the
  dominant chord a tritone above, i.e. the V7 with the bII7
- __auxiliary chord__: a passing chord that is not in the key of the song, used
  to anticipate (resolve to) an in-key chord
- __tonicisation__: resolving an auxiliary chord; the chord that it resolves to
  acts as a temporary new tonic chord
- __secondary function__: the harmonic function of an auxiliary chord relative
  to the chord that it resolves to; e.g. the V/ii chord is the V chord relative
  to the ii chord in the original key
- __secondary dominant__: a dominant 7th chord built on a note that is not the
  5th degree in the key of the piece; has V secondary function
- __harmonic sequence__: chords or groups of chords moving stepwise
- __constant structure__: playing the same type of chord successively on
  different scale degrees
- __circle of 5ths progression__: root movement by perfect 4ths ascending or
  perfect 5ths descending; e.g. vi-ii-V-I or I-vi-ii-V
- __pivot chord__: that exists in two keys and creates smooth transition while
  modulating between those two keys

### Chord voicing

- __block chord__: played as a sound shape, all notes at the same time
- __broken chord__: chord notes are played one-by-one creating a melody line
- __arpeggio__: chord notes are played in succession upwards or downwards
- __voicing__: how notes of a chord are ordered and spaced
- __closed structure__: arrangement of chord notes so that they are the closest
  notes
- __open structure__: arrangement of chord notes so that they are more than an
  octave apart
- __four way close voicing__: all 4 notes of a 6th or 7th chord are played in a
  closed structure
- __shell chord voicing__: the 5th is omitted from a 6th or 7th chord
- __locked hands voicing__: the highest note of a chord is doubled an octave
  lower
- __drop 2 voicing__: the 2nd highest note of a chord is played an octave lower
- __drop 3 voicing__: the 3rd highest note of a chord is played an octave lower
- __pivot arpeggio__: the root note of a chord is played an octave higher when
  played as an arpeggio

## Melody

### Voice leading

- __step / conjunct motion__: movement by a semitone or tone interval
- __leap / skip / disjunct motion__: movement by an interval larger than a tone
- __target tone__: a characteristic note of the underlying harmony that gets
  accented; typically a chord tone
- __passing tone__: a decorating note that is not important for the underlying
  harmony, played briefly and without accent
- __approach tone__: a passing tone that resolves to a target tone
- __neighbour tone__: an approach tone that returns to the original target tone
- __guide tone__: a note of a chord that defines the quality of the chord, i.e.
  the 3rd or the 7th; emphasised in the melody
- __character tone__: a note of a modal scale that defines the mode of the
  scale, e.g. the lowered 7th in the Mixolydian scale; emphasised in the melody
- __dominant approach__: resolving to a target tone from a perfect fifth above
  or below
- __chromatic approach__: resolving to a target tone from a semitone above or
  below
- __scale approach__: resolving to a target tone from a diatonic scale step
  above or below
- __enclosure__: resolving to a target tone through 2 surrounding notes,
  typically a scale step above and a chromatic step below
- __scale-wise motion__: connecting adjacent target tones with diatonic scale
  tones that occur in between
- __chromatic motion__: connecting adjacent target tones with chromatic scale
  tones that occur in between
- __guide tone line__: melody line following the guide tones of successive
  chords as target tones to outline the harmonic motion
- __anticipation__: connecting two chords by playing a note of the second chord
  before the chord arrives
- __suspension__: connecting two chords by holding a note of the first chord
  over into the second chord
- __transposition__: moving a phrase to a different pitch
- __real transposition__: all intervals are exactly maintained
- __tonal transposition__: transposing to another tonal degree in the same key;
  the quality of the intervals may change according to diatonic relationships
- __ostinato / vamp / riff__: repetitions of a phrase at the same pitch
- __variation__: repetition with alterations instead of exact repetition
- __sequence__: variation by transposition
- __inversion__: variation by inverting each interval in a phrase
- __intervallic change__: variation by expanding or contracting intervals
- __augmentation__: variation with longer notes than previously used
- __diminution__: variation with shorter notes than previously used
- __addition / embellishment__: new notes are added to a phrase at each
  repetition
- __motivic development__: constructing phrases from variations of a motif
- __octave displacement__: shifting a note in a phrase up or down an octave and
  continuing the phrase in that new octave

### Bass lines

- __ground bass / basso ostinato / riff__: a steadily repeated bass phrase
- __walking bass__: bass line with steady quarter note movement
- __Alberti bass__: broken chord bass line pattern as root - 5th - 3rd - 5th as
  8th notes
- __disco bass__: bass line with 8th note octave leaps up and back

### Walking bass

- __1 chord per bar__: root + chord note or approach note + chord note + approach
  note; in most simple form: root + 3rd + 5th + chromatic approach
- __2 chords per bar__: chord note + approach note; in most simple form: root +
  chromatic approach

### Counterpoint

- __countermelody__: a second melody line that supports the leading voice
- __accompaniment__: block chords that support the leading voice
- __imitation__: repetitions of a motif or phrase at different voices
- __canon__: multiple voices follow each other, each one imitating the leading
  phrase with an offset
- __heterophony__: multiple voices play variations of a phrase simultaneously
- __antiphon__: multiple voices are distributed in space (at different parts of
  a venue)
- __parallel motion__: two melodic parts move so that they keep the same
  interval
- __similar motion__: two melodic parts move in the same direction
- __oblique motion__: one melodic part remains static while the other moves
- __contrary motion__: one melodic part rises while the other falls
- __cantus firmus__: composition method in which a guide tone line contrasts
  with melodies in other voices
- __drone__: a continuously sounded note through an entire phrase
- __pedal point__: a sustained bass note (drone) against a changing harmony
- __line cliché__: a stepwise moving bass line against a sustained chord
- __call and response__: composition method in which a phrase is followed by a
  variation of the phrase in a different voice

## Composition

- balance of _uniformity_ (keeping the music intelligible) and _variety_
  (keeping the music interesting)

### Harmony

- use inversions and substitutions to keep the same notes when changing from one
  chord to the next
- the tonic chord should most of the time be in root position
- _root movement by 2nd_: subsequent triads have no notes in common → rough
- _root movement by 3rd_: subsequent triads have two notes in common → smooth
- _root movement by 4th_: subsequent triads have one note in common → balanced

### Melody

- leaps in a melody should be followed by steps in the opposite direction,
  unless the leaps outline a triad
- when moving from one note to the next, move to the next closest one (i.e.
  avoid leaps larger than an octave in a melody)
- all scale degrees other than the root and the 3rd should be resolved

### Counterpoint

- avoid tritone and minor 9th between voices
- avoid gaps larger than an octave between adjacent voices (except for the bass
  line that may be further apart)
- avoid voice crossing, i.e. when a voice gets higher or lower than the melody
  in an adjacent voice
- when voices move in the same direction, avoid consecutive fifths and octaves

## Classical music

- __opus / work__: a piece of music
- __sonata__: a piece that comprises of 3 or 4 movements: 1. a dramatic movement
  in sonata form; 2. a slow movement in ternary form; 3. a dance (optional); 4.
  a finale in rondo form
- __symphony__: a sonata written for a large orchestra
- __concerto__: a sonata in which a solo instrument is accompanied by an
  orchestra
- __cadenza__: solo section in a concerto in which only the soloist plays and
  the orchestra remains silent; may be improvised
- __tutti__: orchestral section in a concerto
- __minuet__: a French dance style in 3/4 in A B A form
- __scherzo__: musical "joke", a fast and playful movement
- __fantasia__: a piece without a prescribed form that develops the opening
  theme freely throughout the piece
- __rhapsody__: a piece without a prescribed form that comprises of a series of
  distinct musical ideas with great contrast between them
- __overture__: a piece that comprises of a single movement, to start an opera
  or concert with
- __cantata__: a vocal piece performed by a choir with instrumental
  accompaniment
- __programme music__: a musical piece that tells a story
- __character piece__: a musical piece that portrays a scene
- __absolute music__: music without non-musical references
- __reduction__: transcription of an orchestral piece for a solo instrument
- __atonal music__: music without an identifiable tonal centre for resolving
  tensions
- __serialism__: a composition technique for creating atonal music that gives
  each note of a scale equal priority
- __dodecaphony / 12-tone serialism__: a serial composition technique whereby
  each phrase contains all 12 notes of the chromatic scale in a succession

### Classical form

- __episode__: a section of a movement in which the opening theme is not present
- __prime__: a section that is the same as an earlier section with some
  modifications, notated as A → A'
- __binary form__: a movement that comprises of 2 sections, A and B where B is
  in a different key
- __ternary form__: a movement with 3 sections in the form A B A' where B is in
  a different key
- __fugue__: a movement that is built on imitations of two subsequent phrases
  called the _subject_ and the _answer_ by multiple voices, while a
  _countersubject_ provides counterpoint in another voice
- __fugato__: a section that is in fugue form within a longer movement
- __sonata form__: a movement in ternary form; the 3 sections are the
  _exposition_, _development_ and _recapitulation_
- __exposition__: opening section with 2 contrasting themes (or groups of
  themes) in different keys, connected with a _transition_ phrase
- __development__: intermediate section that varies themes from the exposition
  through modulations to different keys, closed with a _retransition_
- __retransition__: a prolonged harmony (usually the dominant 7th) that leads
  back to the opening theme
- __recapitulation__: final section that repeats the themes of the exposition,
  but both are in the same key, i.e. the contrast is resolved; closed with a
  coda
- __rondo form__: a movement with a returning theme in the form A B A C A D A …;
  the B, C, D, etc. sections are _episodes_
- __theme and variation form__: a movement with in the form A A' A''…, each
  section being a variation of the A section called the _theme_
- __leitmotif__: a motif associated with a person or place in an opera or film

### Symphony orchestra

- __woodwinds__: flutes, oboes, clarinets, bassoons
- __brass__: French horns, trumpets, trombones, tubas
- __percussion__: timpani, snare, bass drum, cymbals, triangle
- __keyboards__: piano, celesta
- __strings__: harps, violins 1, violins 2, violas, cellos, double basses

### Eras

- __ancient__: before 500
- __medieval__: 500 - 1400
- __renaissance__: 1400 - 1600 (Palestrina, Dowland, Monteverdi)
- __common practice__: 1650 - 1900
    - __baroque__: 1600 - 1750 (Vivaldi, Bach, Handel)
    - __classical__: 1750 - 1820 (Haydn, Mozart, Beethoven)
    - __romantic__: 1800 - 1910 (Chopin, Schumann, Liszt, Tchaikovsky)
- __modern__: 1890 - 1950
    - __post-romanticism__: 1890 - 1910 (Mahler, R. Strauss)
    - __impressionism__: 1890 - 1920 (Debussy, Ravel)
    - __expressionism__: 1900 - 1930 (Schoenberg, Webern, Berg)
    - __neo-classicism__: 1920 - 1950 (Stravinsky)
- __contemporary / postmodern__: after 1950
    - __serialism__ (Boulez, Stockhausen)
    - __experimental music__ (Cage)
    - __minimalism__ (Glass, Reich, Riley)
    - __neo-romanticism__ (Pärt)

## Jazz

- __head__: a composed melody that a jazz tune starts with and improvised solos
  are based on; typically 12 bars long (_jazz-blues_), or 32 bars long in AABA
  form (_rhythm changes progression_)
- __combo__: small orchestra
- __big band__: large orchestra with 4 sections: saxophones, trumpets,
  trombones, rhythm section
- __rhythm section__: drums, bass, guitar, piano (keyboards)
- __comping (piano, guitar)__: accompanying an improvised solo responsively with
  chords and counter-melodies
- __comping (drum set)__: complementing an improvised melody responsively on the
  drums, while keeping time on the cymbals
- __vamping__: repeating a short phrase or chord progression (_vamp_ / _riff_)
  by the rhythm section until the soloist joins in
- __fill__: brief solo played by an accompanying instrument to fill the gap when
  the soloist pauses or plays a static melody
- __lead sheet__: a sheet that only includes the chord progression and the head
  melody of a tune, used as an outline for improvisation
- __fake book__: a collection of lead sheets of jazz standards

### Improvisation

- __solo-accompaniment__: the approach that a soloist improvises melodies and
  the other instruments are _comping_ (accompanying) the soloist
- __collective improvisation__: the approach that there is no distinguished
  soloist and all instruments improvise simultaneously (typical in New Orleans
  jazz & free jazz)
- __melodic / motivic improvisation__: building melodies from variations on the
  head or the vocal lines of the song (typical in New Orleans jazz & early
  swing)
- __chordal improvisation__: building melodies freely around the notes of the
  chords of the chord progression
- __chord tone / vertical improvisation__: building melodies using the notes
  of the chord or its substitution or its extension; chord tones are connected
  with approach notes, enclosures and scale runs (typical in swing & bebop)
- __chord scale / horizontal improvisation__: building melodies using the notes
  of selected chord scales for the chord (typical in modal jazz & fusion)
- __common scale improvisation__: building melodies using one scale that fits
  the entire chord progression (typical in rock)
- __free improvisation__: music without a predefined structure that is based on
  spontaneous interaction between the participating musicians
- __sheets of sound__: exploring as many of the possible chord substitution
  arpeggios, superimposed chord arpeggios and chord scales over a chord as
  possible in the same solo (John Coltrane in the 1950s)
- __harmolodics__: philosophy associated with free jazz that rejects tonality;
  melody, speed, rhythm, time and phrases all have equal importance (Ornette
  Coleman)

### Subgenres

- __New Orleans / Dixieland jazz__: dance music based on 2/4 swing rhythm with
  simple melody lines and collectively improvised accompaniment (1910s-20s)
- __swing__: dance music based on 4/4 swing rhythm played by big bands with
  members exchanging individual solos (1920s-40s)
- __bebop__: swing played by small combos with faster tempos using more complex
  harmonies (1940s-50s)
- __cool jazz__: bebop with elements of European classical music, slowed down to
  more relaxed tempos (1950s)
- __hard bop__: bebop with elements of African-American popular music (blues,
  gospel & soul) (1950s-60s)
- __third stream__: cool jazz with strong influence of classical form and
  counterpoint (1950s-60s)
- __modal jazz__: bebop-based jazz where the chord progression consists of long
  extended chords and pedal point harmonies, with few chord changes (1950s-)
- __free jazz__: bebop-based jazz with free improvisation and no prearranged
  structure or chord progression (1960s-)
- __post-bop__: modal jazz with non-functional harmony and non-standard song
  structures (1960s-)
- __bossa nova__: jazz with samba rhythm instead of swing (1960s-)
- __jazz fusion__: modal jazz with straight rock and/or funk rhythm instead of
  swing and simpler harmony (1970s-)
- __crossover jazz__: music of any genre applying elements of jazz harmony and
  improvised solos (1980s-)

### Landmark recordings

- __Louis Armstrong and His Hot Five__ (1925-27): introduction of improvised
  solos
- __Body and Soul / Coleman Hawkins__ (1935): introduction of chordal
  improvisation
- __Savoy and Dial Sessions / Charlie Parker__ (1945-48): introduction of bebop
- __Kind of Blue / Miles Davis__ (1959): introduction of modal jazz
- __The Shape of Jazz to Come / Coleman Hawkins__ (1959): introduction of free
  jazz

## Vocals

- __aria__: vocal solo
- __recitative__: a song in which singing resembles speech
- __lieder__: a poem performed by a singer, accompanied by a piano
- __a capella__: music for voices with no accompaniment
- __libretto__: all text associated with an opera or musical
- __lyrics__: the words a singer sings
- __chest voice__: vocal register in which air feels to be resonating in the
  chest
- __head voice__: vocal register in which air feels to be resonating in the head
- __middle/mixed voice__: transition register between head voice and chest voice
- __vocal break / passaggio__: the boundary between two vocal registers

## Guitar

- __guitar position__: a number that identifies the fret that the index finger
  is aligned to
- __tirando / free stroke__: plucking a string by pulling it against the body
- __apoyando / rest stroke__: plucking a string by pulling it upwards and
  resting the finger on the string above
- __picado__: staccato stroke where the note is immediately silenced after
  plucking the string, used in flamenco
- __pizzicato / palm mute__: staccato stroke where the string is muted with the
  right hand palm
- __slap__: plucking a string by hitting it with the side of the right hand
  thumb
- __pop / Bartók pizzicato__: plucking a string by pulling it away from the body
  and letting it bounce back and hit the fingerboard
- __slur__: playing multiple notes with a single pluck of a string
- __hammer-on__: slur upwards
- __pull-off__: slur downwards
- __smear / fall-off__: a quick slide downwards, closing a phrase
- __tremolo__: plucking a string rapidly and monotonously, creating a drone-like
  sound
- __rasgueado__: rhythmic strumming of block chords with one finger at a time,
  mostly used in flamenco
- __sul ponticello__: instruction to pluck the strings near the bridge
- __sul tasto__: instruction to pluck the strings over the fingerboard
- __campanella__: arranging notes of a melody on the fretboard so that they are
  able to ring out against each other, resulting in a sustain pedal effect
- __scordatura__: non-standard tuning, notated as if the guitar was in normal
  tuning
- __natural harmonic__: producing harmonic overtones on an open string over the
  fretboard at an interval from the _overtone series_ of the string
- __artificial harmonic__: producing harmonic overtones on a fretted string

### Finger notation

- left hand: _1 - 4_ for index, middle, ring and pinky fingers
- right hand: _p, i, m, a_ for thumb (pulgar), index (indico), middle (medio)
  and ring (anular) fingers
- string numbering: the 1st string is the highest in pitch / lowest in position

## Piano

### Finger notation

- _1 - 5_ for thumb, index, middle, ring and pinky fingers
- avoid using the thumb for black keys if possible

### Pedals

- __sustain / damper pedal__: lifts all string dampers up, sustaining any note
  after their keys are released
- __soft pedal__: reduces the number of strings being hit by a hammer from 3 to
  2 or 1
- __sostenuto pedal__: keeps the string dampers lifted off the keys being
  pressed at the time the pedal is pressed, sustaining those notes but not
  sustaining any other note

### Pedaling techniques (sustain pedal)

- __delayed pedal__: pressed immediately after playing the first note of a
  harmony and released just before the harmony changes
- __simultaneous pedal__: pressed and released at the same time as a key, to add
  emphasis to certain notes
- __preliminary pedal__: pressed before playing a note, to add a dramatic effect
  to an opening chord

### Red-blue method of building scales

- __blue notes__: C D E | F#(Gb) G#(Ab) A#(Bb) (the 3 white keys from the C-E
  group + the 3 black keys from the F-B group)
- __red notes__: Db(C#) Eb(D#) | F G A B (the 2 black keys from the C-E group
  and the 4 white keys from the F-B group)
- each major scale is made up of 3 adjacent notes of the root note's colour and
  4 of the other colour moving upwards from the root
- each minor scale is made up of 2 adjacent notes of the root note's colour, 3
  of the other colour, and 2 of the first colour

## Percussion

- __fulcrum__: the grip on the drumstick by the thumb and index fingers
- __rudiment__: a simple rhythm pattern that more complex patterns are built
  from
- __stroke__: a note performed on a drum
- __full stroke__: a stroke from high hand position to high hand position
- __down stroke__: a stroke from high hand position to low hand position
- __up stroke__: a stroke from low hand position to high hand position
- __tap__: a stroke from low hand postition to low hand position
- __double stroke__: two successive strokes played by the same hand
- __diddle__: a double stroke played at the speed of the context
- __paradiddle__: two single strokes followed by a double stroke played at the
  speed of the context
- __drag__: a double stroke played at twice the speed of the context
- __flam__: two single strokes, the first is a grace note and the second is an
  accented note
- __roll__: sustained sound by allowing the stick to bounce multiple times after
  a stroke
- __single stroke roll__: roll with 1 stroke per hand alternating between left
  and right
- __open / double stroke roll__: roll with 2 strokes per hand alternating
  between left and right
- __closed / multiple stroke / buzz roll__: roll with multiple short notes per
  hand alternating

### Grip types

- __traditional grip__: the left grip is _overhand_, the right grip is
  _underhand_
- __matched grip__: the two hands mirror each other
- __German grip__: palms are horizontal, drumsticks are perpendicular
- __French grip__: palms are vertical, drumsticks are parallel
- __American grip__: palms are at 45° to the ground, drumsticks are at 45°

## String instruments

- __fretting__: pressing a string against a fret at a specific interval on
  fretted instruments
- __stopping__: pressing a string against the fingerboard at a specific interval
  on fretless instruments
- __position__: an alignment of fingers on the fingerboard
- __double stop__: playing two notes simultaneously on a bowed instrument
- __course__: a group of adjacent strings tuned to the same pitch on some
  plucked instruments, played together to sound a single note

## Wind instruments

- __concert pitch__: the sounding pitch of a note that an instrument plays
- __transposing instrument__: for which the pitch of a written note is different
  from the concert pitch; allows using the same fingering on different
  instruments
- __Bb instrument__: sounds one whole tone lower, i.e. a written C sounds as Bb
  (e.g. tenor & soprano saxophone & clarinet)
- __Eb instrument__: sounds a minor 3rd higher, i.e. a written C sounds as Eb
  (e.g. alto saxophone & clarinet)
- __multiphonics__: a technique that produces 2 notes at one time on a
  monophonic instrument
- __circular breathing__: a technique that produces a continuous sound without
  interruptions for taking breaths
- __overblowing__: a technique that makes the pitch change to an upper register
  (typically an octave higher) due to blowing harder
