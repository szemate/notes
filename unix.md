# Unix Notes

## Directory structure convention

- `/bin`: system binaries
- `/boot`: boot record
- `/dev`: devices _(logical file system)_
- `/etc`: system configuration
- `/home`: users' homes
- `/lib`: common libraries
- `/mnt`: mounted volumes
- `/opt`: applications not following Unix deployment conventions
- `/proc`: process and hardware information _(logical file system)_
- `/root`: the superuser's home
- `/run`: runtime data not preserved across reboots _(in-memory file system)_
- `/sbin`: system binaries that require superuser privileges
- `/srv`: services
- `/sys`: access to kernel data structures _(logical file system)_
- `/tmp`: temporary files _(in-memory file system)_
- `/usr`: appilcations installed by the package manager
- `/usr/local`: applications not installed by the package manager
- `/var`: runtime data preserved across reboots

## Config files

### System

- `/etc/crontab`: list of scheduled tasks
- `/etc/exports`: list of directories shared on network
- `/etc/fstab`: list of filesystems mounted at boot
- `/etc/group`: list of user groups and their members
- `/etc/hosts`: list of known hosts in the local network
- `/etc/init.d/`: startup scripts of daemons
- `/etc/networks`: list of known networks accessible from the computer
- `/etc/passwd`: list of users and their passwords
- `/etc/rc*.d/`: startup scripts by runlevel
- `/etc/services`: mapping services to connection descriptors (port, protocol)

### User

- `~/.bash_profile`: script that is executed when the user logs in
- `~/.bashrc`: script that is executed when the user starts a new shell
- `~/.ssh/`: the user's SSH keys and configuration

### Linux-specific

- `/etc/cron.*/`: hourly, daily, monthly sheduled tasks
- `/etc/sysconfig/`: various configuraton files not in Unix specification
- `/etc/systemd/`: global service unit configuration
- `/etc/udev/rules.d/`: device management rules
- `~/.config/systemd/user/`: user-level service unit configuration

## Standard (pre-connected) streams

- `/dev/stdin`: standard input; file descriptor 0
- `/dev/stdout`: standard output; file descriptor 1
- `/dev/stderr`: standard error; file descriptor 2

## Runlevels

- Runlevel _0_: shutdown
- Runlevel _1_: single user, no network services
- Runlevel _2_: multi-user, no network services
- Runlevel _3_: default console mode
- Runlevel _4_: reserved for later use
- Runlevel _5_: default GUI mode
- Runlevel _6_: reboot

## Inter-process communication

- __Pipe__: unidirectional (FIFO) byte stream, local to a machine
- __Domain socket__: bidirectional byte stream, local to a machine
- __IP socket__: bidirectional byte stream between machines

## File types

- `-`: regular file
- `d`: directory
- `l`: symbolic link
- `p`: named pipe
- `s`: socket
- `c`: character device
- `b`: block device

## File permissions

- *r*ead / *w*rite / e*x*ecute
- Column 1: file owner's permissions
- Column 2: group members' permissions
- Column 3: everyone else's permissions

### Octal format

```
    0 ---
    1 r--
    2 -w-
    3 rw-
    4 --x
    5 r-x
    6 -wx
    7 rwx
```

## Signals

- `SIGHUP` (1): parent terminal got closed
- `SIGINT` (2): keyboard interrupt (Ctrl + C)
- `SIGABRT` (6): abnormal termination (`abort` system call)
- `SIGKILL` (9): immediate termination (cannot be caught by a user process)
- `SIGSEGV` (11): segmentation fault
- `SIGPIPE` (13): broken pipe
- `SIGALRM` (14): timer expired (`alarm` system call)
- `SIGTERM` (15): graceful termination

- The exit code of a process that caught a signal is _128 + signal number_
- A signal sent to `-PGID` _(-1 × process group ID)_ is sent to all members of
  the process group

## Tools

- `strace`: trace system calls of a process
- `host`, `dig`, `nslookup`: DNS look-up
- `ifconfig`: network interface configuration
- `iptables`: packet filtering & NAT
- `lsof -i`: show open network files
- `ncat`: read/write network sockets
- `netstat`: show open network sockets
- `nmap`: port scan
- `ping`: echo request
- `route`: show the routing table
- `socat`: transfer data between sockets
- `tc`: traffic shaping
- `time`: measure how long a command runs
- `traceroute`: show hops of a packet's journey
- `tcpdump`: intercept network traffic
- `ssh-keygen`: generate SSH keys
- `ssh-copy-id`: set up passwordless login for a local user on a remote machine
- `watch`: execute a command periodically

## Usual keyboard shortcuts (in most shells)

- Ctrl + L: clear screen
- Ctrl + C: send `SIGINT` to the foreground process
- Ctrl + Z: suspend the foreground process and send it to the background
- Ctrl + D: terminate the standard input of an interactive process (send `EOF`)
- Ctrl + Alt + F#: switch terminals
