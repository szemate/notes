# Pedagogy Notes

## Mental digestive process of making ideas (Young)

1. _Bound_: establish boundaries
2. _Ingest_: gather information
3. _Arrange_: reflect on it
4. _Stop_: let the subconscious mind work
5. The idea "magically" appears

## SMART criteria of setting goals (Drucker)

- Specific
- Measurable
- Achievable
- Relevant (i.e. significant for you)
- Time-bound

## 5 Second Rule of productivity (Robbins)

- Have an idea (push moment)
- Act on it within 5 seconds whether you feel like it or not (activation energy)
- If you fail to act within 5 seconds, your mind will talk you out of it

## Implementation intention (Gollwitzer)

- Making a plan makes it more likely that we'll act on reaching a goal
- Sharing our goal with someone who acknowledges it makes it less likely we'll
  act on it (sharing already makes us feel accomplished)

## PQRST method of studying (Atkinson)

1. Preview
2. Question
3. Read (and Reflect)
4. Self-recite
5. Test

## Reverse learning (de Bono)

For memorising speeches, poems or music:

1. Read out (play) the entire piece from start to end
2. Read out (play) the last sentence (line, phrase) of the piece several times
3. Recite it 3 times correctly without looking at the source
4. Move on to the second-last sentence (line, phrase) and learn until the end
5. Repeat until the start of the piece

## Mindsets (Dweck)

- __fixed mindset__: we're born with a fixed level of intelligence, the purpose
  of learning is to be smart
- __growth mindset__: intelligence can be developed with effort, the purpose of
  learning is to grow
- Learners should neither be praised for pure effort nor for being clever but
  for their continuous improvement

## Learning styles (Fleming)

- __visual__: most easily learn what they see
- __auditory__: most easily learn what they hear
- __kinesthetic__: most easily learn while they move

## Learning styles (Honey & Mumford)

- __activist__: the purpose of learning is having an experience
- __reflector__: the purpose of learning is reflecting on an observation
- __theorist__: the purpose of learning is drawing a conclusion
- __pragmatist__: the purpose of learning is putting theory into practice

## Bloom’s taxonomy

Levels of understanding are:

1. Knowledge
2. Comprehension
3. Application
4. Analysis
5. Synthesis
6. Evaluation

## Classroom engagement techniques

- __cold calling__: asking a random student to answer a question
- __call and response__: asking all students to answer a question in unison
- __everybody writes__: asking a question and leaving time for students to
  prepare the answer
- __targeted questioning__: addressing questions of various difficulty levels to
  specific students based on their abilities
- __everyone responds__: asking all students to react to a question, e.g.
  nodding or shaking their heads
- __popcorning__: a student gives an answer to a question and asks another
  student to give their answer

## Social rules of teaching (Hacker School Manual)

- _No feigning surprise_: don't act surprised if a student doesn't know
  something that they "should"
- _No well-actuallys_: don't correct students if they give an answer that is
  almost but not entirely correct
- _No back-seat driving_: don't half-participate in a group's work; only offer
  your advice if you can fully engage
- _No subtle -isms_: avoid slips of the tongue that may make people feel
  unwelcome (e.g. "it's easy")

## Constructivist teaching

- __development__: spontaneous process initiated by the learner, the result of
  independent thinking (Piaget)
- __zone of Proximal Development__: the skills and knowledge the learners do not
  own yet but are within reach in the next step of learning (Vygotsky)
- __cognitive apprenticeship__: the process in which a master of a skill
  demonstrates an activity, and apprentices observe and imitate the master

## Problem-based learning

- __socratic method__: the mediator asks questions about a topic that help
  learners distinguish what they understand from what they don't
- __enquiry-based learning__: students pose questions and define problems about
  a topic in a mediated group discussion in order to draw a conclusion
- __anchored instruction__: students freely explore and define problems around a
  theme called the 'anchor'
- __interleaved practice__: instead of teaching a specific problem type at once
  and then moving on to a different problem type, they are spaced out and mixed
  with other problem types

## Cooperative learning

- __reciprocal teaching__: students alternate roles as learners (posing
  questions) and teachers (collecting evidence and answering the questions)
- __jigsaw__: students become experts on one part of a group project and teach
  it to the others
- __structured controversies__: students work together to research a topic

## Flipped classroom teaching method (Salman Khan)

- Pupils watch lectures and read at home
- In class they ask questions and practice 
- Teachers are not instructors but coaches/mentors

## Feynman technique of studying

1. Study a subject
2. Imagine teaching it to someone, as if in a class
3. Do the same but using as simple language as possible

## Explain & expand technique

- Method of checking for understanding of a technical subject
- Ask the student to explain their solution to an exercise
- Expand the problem with a small additional exercise and ask the student how
  they would solve it

## Multiple intelligences (Gardner)

- visual – spatial
- musical – rhythmic
- verbal – linguistic
- logical – mathematical
- bodily – kinesthetic
- interpersonal
- intrapersonal
- (naturalistic)

## PEA framework for giving feedback

Good feedback is:

- precise
- evidence-based
- actionable

## Giving feedback (LeeAnn Renninger)

1. _micro-yes_: ask permission to give feedback (e.g. "Do you have 5 minutes to
   talk about X?", "Can I share my ideas about Y?")
2. _data point_: be objective and specific, avoid _blur words_
3. _impact statement_: how the data point impacted you (or the team or the
   organisation)
4. _question_: ask the feedback taker's opinion

## Drive theory of job satisfaction (Pink)

- __autonomy__: the urge to be self-directed
- __mastery__: the urge to get better skills
- __purpose__: the urge to do something that has meaning

## Self-determination theory of motivation (Deci & Ryan)

### Innate needs (ARC)

- __autonomy__: the perception that we have choices and we are the source of our
  actions
- __competence__: the need to develop and demonstrate our skills
- __relatedness__: the need to care and being cared about

### Types of motivation

- __intrinsic__: driven by innate needs, optimal
- __extrinsic__: driven by rewards or punishment, suboptimal

## Motivational outlooks (Facer & Fowler)

### Suboptimal

- __disinteresred__: finding no value in an activity
- __external__: participating in an activity for rewards or to avoid punishment
- __imposed__: participating in an activity because of peer pressure

### Optimal

- __aligned__: participating in an activity because it aligns with one's values
- __integrated__: participating in an activity because it is linked to one's
  purpose (meaning)
- __inherent__: participating in an activity because of the enjoyment (flow)
- People with integrated and aligned motivational outlooks have high quality
  *self-regulation*

## Transfer of learning

- The process of applying something one has learnt to a new situation
- __near transfer__: to a similar situation or context
- __far transfer__: to a completely different situation or context

## Types of fake knowledge

- __idle knowledge__: that you can recall but unable to apply (other than in
  exams and quizzes)
- __ritual knowledge__: that you can apply but no longer remember the meaning of
- __double knowledge__: that you can apply in safe situations but ignore when
  solving genuine problems
- __foreign knowledge__: that is accumulated about a topic without relating to
  it

## Heuristic techniques

- Trial and error
- Rule of thumb
- Educated guess
- Intuition
- Common sense
- Profiling

## Signaling model of education (Bryan Caplan)

- Most of higher education doesn't teach job skills, transferable skills or work
  habits
- Nevertheless to get high grades one has to be smart, hard working and
  conformist
- Differences between applicants for a job are difficult to observe
- The main purpose of the higher education system is to allow prospective
  employees show off their productivity through achievements in wasteful
  activities
