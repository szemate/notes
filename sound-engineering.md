# Sound Engineering Notes

## Three dimensions of a sound in engineering [position on the soundstage]

- dynamic range [front to back]
- frequency content [top to bottom]
- stereo pan position [left to right]

## Three dimensions of a sound in music

- pitch
- duration
- dynamics

## Sound engineering process

1. tracking
2. mixing
3. mastering

## Analysis tools

- oscilloscope / waveform meter
- spectrum analyser
- sonogram / spectrogram
- Lissajous meter

## Psychoacoustic effects

- Haas effect
- equal-loudness (Fletcher-Munson) curves
- auditory masking

## Sound engineering compromises

- __gain staging__: setting gain and dynamic range at each stage of the signal
  chain high enough for good SNR but low enough to avoid distortion
- __frequency shaping__: identifying and filtering/panning conflicting (masking)
  frequencies between tracks
- __mixing__: due to the uneven frequency response of the human ear the
  perceived levels are different at high volume and low volume
- __limiting / compression__: increasing the overall loudness and punchiness of
  a mix without adding unwanted harmonics and flattening out the dynamics

## Terms

- __Q__ = dB/octave
- __ADSR__ = Attack time, Decay time, Sustain level, Release time
- __ARRT__ = Attack time, Release time, Ratio, Threshold level

## Compressor setup

1. _attack_: start at minimum
2. _release_: start at minimum
3. _ratio_: start at maximum
4. _threshold_: start at an extreme setting (around -20 dB)
