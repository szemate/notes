# Drawing Notes

## Elements of drawing

- edge (line)
- shape (2D)
- form (3D)
- value
- texture
- space (+ negative space: the shape *around* an object)

## Elements of anatomy

- proportions
- bones
- muscle groups
- flexibility and joint limitations

## Basic forms

- sphere
- cylinder
- cube

### Cylinders (and cones)

- The major axes of the ellipses are perpendicular to the central axis of the
  cylinder
- The major axis of the ellipse follows the contour of the surface
- Elliptical contour lines have more curvature near the edges
- Convex and concave forms have opposite light patterns

## Steps of a drawing

1. Multiple thumbnail sketches
2. Rough sketch
3. Fixes on the rough sketch
4. Refined drawing

## Steps of a sketch

1. Envelopes (framing & boundaries of the subject)
2. Gesture
3. Block-in (outlines of masses)
4. Hard contours
5. Soft contours
6. Shading
7. Refinement

## Gesture drawing

- __beans__: simplified masses
- __gesture lines__: simplified contours
- __line of action__: a continuous line that shows the flow of a gesture
- Shapes of gesture lines: _C_, _S_, _I_

## Steps of measurement

1. Establish the unit
2. Measure proportions (compared to the unit)
3. Draw guide lines
4. Mark lower & upper limits and midpoints of shapes
5. Check symmetry

## Taking measurements

- Hold arm straight
- Tilt head to keep eye close to shoulder
- Close one eye
- Use tip of the pencil as one point and thumb as the other point
- Measure horizontally or vertically (not at an angle)

## Light logic

- __shadow line / terminator__: separates light and shadow areas
- __highlight__: the brightest spot in direct light (the highest value)
- __centre light__: the surface in direct light
- __mid-tone / half-tone__: the surface in diffuse light
- __core/crest/form shadow__: cast by an object on itself
- __reflected light__: bounced back from surrounding objects, which lightens the
  core shadow
- __cast shadow__: cast by an object on surrounding objects
- __occlusion/accent shadow__: the darkest spot in cast shadow near the point of
  contact (the lowest value)
- The light source is on the line that connects the end of cast shadow and the
  highlight
- Contour lines are thick in shadow areas and thin in light areas

## Steps of shading

It's worth working on a middle grey background to see mid-tones accurately

1. terminator
2. lights and shadows (2 values)
3. dark & light accents (the highest & lowest values)
4. mid-tones & gradations

## Shaping cast shadow

1. Locate the point below the light source on the surface (for the Sun the point
   is on the horizon)
2. Draw lines from this point through the outer edges of the object touching the
   surface
3. Draw lines that start from the light source and touch the top of the object
4. Connect the intersections, following the object outline

## Shading techniques

- __blending__: merging shades together for smooth transition; the amount of
  pressure applied to the pencil controls value
- __rendering__: removing graphite with an eraser; the amount of pressure
  applied to the eraser controls value
- __hatching__: straight lines; line density controls value
- __cross-contour__: lines that follow contours of the form; line density
  controls value
- __cross-hatching__: evenly spaced crossing lines; the number of layers
  controls value
- __scribbling__: random circular crossing lines; the number of layers controls
  value
- __stippling__: small dots; dot density controls value

## Perspective

### 1-point

- 1 vanishing point on the horizon
- Vertical lines appear vertical

### 2-point

- 2 vanishing points on the horizon
- Vertical lines appear vertical

### 3-point

- 2 vanishing points on the horizon + 1 above or below
- _looking up (worm's eye view)_: the 3rd VP is above horizon
- _looking down (bird's eye view)_: the 3rd VP is below horizon
- Vertical lines point towards the 3rd VP

### 4-point

- 2 vanishing points on the horizon + 1 above + 1 below
- Vertical lines curve towards the 2 VPs

## Animation

- __parallax__: as the viewer moves, objects viewed along different lines of
  sight are displaced at different rates (e.g. the foreground moves more than
  the background)

## Colour wheel

### Primary colours

- red
- yellow
- blue

### Secondary colours

- orange = red-yellow
- green = yellow-blue
- violet = blue-red

### Tertiary colours

- red-orange
- yellow-orange
- yellow-green
- blue-green
- blue-violet
- red-violet

## Components of colour

- __hue__: the pure (root) colour
- __saturation / chroma__: intensity; how bright or muted a colour appears
- __value__: how light or dark a colour appears
- __black__: the lowest value of any colour
- __white__: the highest value of any colour
- __grey__: the lowest saturation of any colour

## Colour mixing

- __tint__: any hue with white added; changes value
- __shade__: any hue with black added; changes value
- __tone__: any hue with either grey added, or its complementary added; changes
  saturation
- __neutralising__ a colour: decreasing its saturation
- Colours get along with each other if they have clear differences in value

## Palettes

- __monochromatic__: variants of the same hue
- __analogous__: 2 to 5 adjacent hues in the colour wheel
- __complementary__: 2 hues on opposite sides in the colour wheel
- __split complementary__: a hue and the 2 neighbours of its complementary
- __triadic__: 3 hues at equal distance (i.e. on an equilateral triangle) in the
  colour wheel
- __compound__: 2 adjacent hues and their complementaries

## Method for colouring illustrations

1. Draw a line image
2. Shade with values from a grey value scale
3. Create a palette
4. Replace each value of grey with a colour that has the same value

## Head proportions

- from eye line to bottom of chin = from eye line to top of skull
- from hairline to eyebrow ridge = from eyebrow ridge to base of nose = from
  base of nose to bottom of chin
- head width = 5 eyes
- width of space between eyes = 1 eye width
- eyebrow width = 1 1/2 eye width
- mouth width: distance between irises
- mouth line: 1/3 way between bottom of nose and bottom of chin
- hairline: at top of forehead
- jawline: at bottom of lower lip
- widest part of head: at 1/3 head length
- widest part of chin: 1/2 way between eye line and base of nose

## Face recognition pattern

- head and hair shape
- size and position of eye sockets, nose and lips
- shadow shapes of eye sockets, lips, jaw and base of the nose are sufficient
  for recognition

## Mannequin model

### Masses

- head (small)
- ribcage (large)
- pelvis (medium)

### Proportions

- neck + ribcage mass + lumbar spine: 2 heads long
- ribcage mass: 1 1/3 heads long
- pelvic mass: 1 head long
- upper leg: 2 heads long, starting at the middle of pelvic mass!
- lower leg & foot: 2 heads long
- arm excluding hand: 2 1/2 heads long

### Joints

- neck
- lumbar spine (lower back)
- 2 shoulders
- 2 hips
- 2 elbows
- 2 wrists
- 2 knees
- 2 ankles

## Balance

- __centre of Gravity__: behind the navel
- __centre of Support__: the centre of mass of the shape defined by points in
  touch with the ground
- The figure looks balanced if the CoG and the CoS are on a vertical line

## Age differences

### Children

- Cranium and eye sockets are adult size
- Jaw is relatively small (centre line is below halfway)

### Elder

- Bone forms are more visible through skin

## Gender differences

### Women

- Straight forehead and upper jaw
- Flat eyebrow ridge
- Eyebrow shape follows the upper edge of eyebrow ridge
- Proportionally larger eyes and lips
- Thin and long neck
- Pelvis is wider than ribcage
- Short collar bone
- Waistline is level with navel
- Hip joints are further apart

### Men

- Slanted forehead and upper jaw
- Pronounced eyebrow ridge
- Eyebrow shape follows the lower edge of eyebrow ridge
- Proportionally smaller eyes and lips
- Thick and short neck
- Ribcage is wider than pelvis
- Long collar bone
- Waistline is below navel
- Hip joints are closer

## Ethnic differences

- __facial angle__: the angle of the line that connects the base of nose and
  bottom of chin, and the line that connects the base of nose and jaw joint

### African

- Facial angle ~ 70˚
- Broad lips and nose
- Eye is set backward in the eye socket
- Upper arm is shorter than forearm
- Upper leg is shorter than lower leg
- Women's shoulders are broader than hips
- Curly hair

### Asian

- Facial angle ~ 80˚
- Narrow lips, broad nose
- Eye is set forward in the eye socket
- Upper arm is longer than forearm
- Upper leg is longer than lower leg
- Women's shoulders are as wide as hips
- Straight hair

### Caucasian

- Facial angle ~ 90˚
- Narrow lips and nose
- Eye is set backward in the eye socket
- Upper arm is longer than forearm
- Upper leg and lower leg are equal length
- Women's shoulders are narrower than hips
- Variations in hair
