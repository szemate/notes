# Philosophy Notes

## Approach

- __Science__: a search for answers
- __Philosophy__: thinking about questions

## Fields

- __Metaphysics__: the nature of reality
    - __Essentialism__: belief that everything and everyone has a preexisting
      essence that gives them function and purpose
    - __Existentialism__: belief that existence precedes essence; nothing and
      no-one has predetermined purpose
- __Epistemology__: the nature and scope of knowledge
    - __Skepticism__: questioning whether anything can be known with certainty
    - __Rationalism__: reason is the most reliable source of knowledge
        - "Cogito ergo sum" (I think therefore I am — Descartes)
    - __Empiricism__: sense and experience are the most reliable sources of
      knowledge
        - "Esse est percepi" (To be is to be perceived — Berkeley)
    - __Transcendentalism__: subjective intuition is the most reliable source of
      knowledge
- __Ethics__: evaluation of human conduct
- __Aesthetics__: the nature of beauty
- __Logic__: tools for reasoning

## Science vs. pseudo-science (Popper)

- __Pseudo-scientific theory__: valid as long as it's proven right
- __Scientific theory__: valid until it's proven wrong
    - _Testable_
    - _Refutable_
    - _Falsifiable_
