# Singing Notes

- **Key**: the right amount of closure, i.e. optimal airflow

## Phonation exercises

- smile <-> duckface
- many small kisses <-> one large kiss
- tongue circling around the teeth
- tongue circling around the lips
- tongue swinging (tip of the tongue is behind the lower front teeth)

## Vocal exercises

- yawning (throat warm-up)
- ha-ha-ha (abs control)
- hissing ('sss' sound)
- lip rolling ("motorboat")
- raspberry trill (lip rolling with tongue in between lips, soft → strong → soft)
- humming ('mmm' sound)
- m-n-ng (humming with different mouth shapes)
- yah-yah (tongue on the lower lip, fixed jaw)
- mom-mom (crying effect)
- nghe-nghe (whining effect)
- goo-goo (wacky sound)
- uh-uh (staccato, jumping from note to note)
- siren (slide up and back)

## Position

- Stand with your back straight
- Hold your head straight (no tension in the neck)
- Don't raise your shoulders when breathing in
- Keep your chest expanded while breathing out and push the air bottom-up
- Open your mouth wide

## Tips

- Imagine breathing into your pelvis
- Imagine singing to a fixed remote object (as if a thread was pulling your breath)
- Slide to the centre of pitch from above, not from below
- When you're singing in the high end of your range, think low
- When you're singing in the low end of your range, think high
