# Fencing Notes

## Attributes of garde positions

- __high / low__: by direction relative to the horizontal fencing line
- __inside / outside__: by direction relative to the vertical fencing line
- __supinated / pronated__: by rotation of the wrist

## Main (supinated) garde positions

- __quarte (4)__: High and to the inside
- __sixte (6)__: High and to the outside
- __septime (7)__: Low and to the inside
- __octave (8)__: Low and to the outside

## Parries

- __lateral__: 6 to 4
- __circular__: 6 to 6
- __diagonal__: 6 to 7
- __semicircular__: 6 to 8

## Targets

- __near__: front arm and leg
- __middle__: trunk and head
- __far__: back arm and leg

## Attributes of attacks

- __direct__: starts and finishes in the same line
- __indirect__: starts and finishes in different lines
- __simple__: doesn't involve feints (faking an attack)
- __compound__: involves feints (faking an attack)

## Lunges

- __explosive__: for simple attacks
- __accelerating__: for compound attacks, slow during the initial feint
- __step and lunge__: step forward for the feint and lunge for the finishing
  attack
- __ballestra__: the lunge is preceded by a jump to close the distance
- __flèche__: attack with a sprint instead of a lunge

## Attacks

- __straight attack (absence de fer)__: blades don't touch
- __beat__: hit your opponent's blade to get it out of the way
- __glide (coulé)__: slide your blade forward along your opponent's
- __flick__: hit your opponent's near target with a flick of your wrist
- __taking the blade (pris de fer)__: push your opponent's blade to a different
  line
- __disengage__: change line under your opponent's blade with a circular motion
  to get from an uncovered position to a covered position
- __cutover (coupé)__: change line over your opponent's blade by turning your
  wrist
- __broken time attack__: pull your arm back to avoid the parry, then extend
  again
- __beat-disengage__: beat, wait for your opponent to beat back and avoid their
  blade
- __counter-disengage__: avoid your opponent's blade when they disengage
- __one-two attack__: disengage followed by counter-disengage

## Phrases

- __parry-riposte__: A attacks, B defends and counterattacks
- __stop hit__: A attacks, B attacks A's near targets
- __remise__: A attacks, B defends, A stays in the lunge and repeats the attack
- __reprise__: A attacks, B defends, A recovers and attacks again

## Preparations for attack

- Close the distance
- Change rhythm
- Close the distance with your near targets exposed to provoke an attack
- __apel__: stomp with your front leg to distract your opponent

## Exploring your opponent (reconnaissance)

- Feel of the blade (sentiment de fer)
    - _light grip_: glide attack
    - _strong grip_: disengage attack
- Reaction to beat
    - _opens to beat_: direct beat attack
    - _beats back_: beat-disengage attack
- Grip position
    - _normal/high_: disengage
    - _low_: cutover

## Tips

- Control the point with your fingers, not your wrist or arm
- Keep eye contact with your opponent
- Start attacks with extension
- Keep your centre of gravity low
- Keep the distance
- Mind your garde position to keep your lower arm protected
- Go for the blade before starting an attack
- Always direct the point of the blade towards the target
- Keep your hand higher than the point when your arm is extended
- Do explosive lunges as if someone was pulling your sword
