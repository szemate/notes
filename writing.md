# Writing Notes

## Tools

- __morning pages__: write 3 pages on paper every morning about whatever crosses
  your mind, to kickstart your daily routine of writing (Julia Cameron)
- __artist dates__: go on a solo exploration once a week and take notes, to
  boost creativity (Julia Cameron)
- Write a story _3 times_: the 1st time to understand it, the 2nd time to
  improve the prose, the 3rd time to make it compelling (Bernard Malamud)

## Voice

- _simplicity_: makes writing punchy (e.g. active voice over passive)
- _clarity_: makes writing easy to understand
- _elegance_ (rhythm and structure): makes writing flow well
- _evocativeness_ (imagery): makes writing stimulating

## Style

- _classic_: conversation between the writer and the reader through a shared
  vision of reality
- _practical_: transparent; draws attention away from the writing to the subject
- _reflexive_: writing about the writing itself (e.g. signposting; commenting on
  the reliability of a source)
- _rhetorical_: conveys a single viewpoint

## Structure

- introduction
    - background
    - thesis statement
    - summary of the outline
- main body
    - essays
        - argument
        - possible objections
        - replies to the objections
    - narratives
        - setup
        - conflict
        - resolution
- conclusion
    - restate the thesis
    - review
    - comment

## Process

- __discovery__: 1st stage; for yourself
    - brainstorm
    - research
    - outline: via headings and sub-headings
    - _vomit draft_: a quick and dirty first draft
- __presentation__: 2nd stage; for the intended audience
    - draft
    - rewrite (multiple times)
    - add references

## Strategies to overcome block

- Create a task list
- Write from the middle out
- Write in the form of a dialogue or conversation
- Write in the voice of a favourite author

## Rules of storytelling

- Skip introductory remarks, dive into the story instead
- The story should revolve around a strong conflict
- Show the change in the character as a result of the conflict
- Provide specific details (e.g. about the characters' appearance)
- Show, don't tell using sensory details (_VAKOG_)
    - _visual_: seeing
    - _auditory_: hearing
    - _kinesthetic_: feeling
    - _olfactory_: smelling
    - _gustatory_: tasting
- Use dialogue, not narration
- Wrap up with a takeaway message
