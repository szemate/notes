# Typography Notes

## Concepts

- __font__ = _typeface_ + style (weight, slant) + size
- units of measurement: 1 _pica_ = 12 points = 1/6 inch
- type classes: _serifed_ vs. _sans-serif_; _proportional_ vs. _monospace_
- guide lines: _baseline_, _x-line_, _cap_(ital) _height_
- __serif__: small line at the end of a stroke in a character
- __ascender__: the part of a character that ascends cap height
- __descender__: the part of a character that descends the baseline
- __leading__: the vertical space between baselines in a block
- __tracking__: the horizontal space between characters in a block (i.e.
  density)
- __kerning__: changing the horizontal space between two individual characters
- __glyph__: the representation of each character in a typeface
- Alignment: _flush left/right_ vs. _justified_ vs. _centred_
- __widow__: lines of text from a paragraph that transition to the next page
- __orphan__: a single word in the last line of a paragraph
- document structure: _heading_, _subheading_, _body_
- matching fonts: use opposites (in serif, style, size, capitalisation method)
- __hero font__: large font used on a banner that is placed prominently to the
  centre of a page

## Rules

- Don't use more than 3 typefaces in a document
- Don't mix visually similar typefaces
- Don't mix typefaces from the same family
- Don't use all caps
- Don't mix size & weight within a section
- Don't alter spacing within a document
- Don't distort the typeface
- Don't choose font colours that blend with the background
- Don't have widows and orphans
