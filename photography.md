# Photography Notes

## Composition rules

- Simplicity
    - Number of image elements <= 4
    - Low internal complexity of image elements
    - Clear separation of image elements
    - A primary visual element
- Asymmetry
    - Vertical asymmetry (top vs bottom)
    - Horizontal asymmetry (left vs right)
    - Asymmetry between the four quadrants
- Eye lines (any element that moves the eye)
    - Physical objects
    - Perspective (fore, middle, background)
    - Tonal & colour transitions
    - Orientation (portrait or landscape)
- Interesting point of view
    - Perspective (human eye, higher, lower, through natural frames)
    - Angle (straight, upwards, downwards, rotated)
    - Amount of detail (near, far)
- Avoid areas in the image with
    - Clutter (high internal complexity)
    - Little visible detail
    - Dead space
    - Crossing of high contrast edges

## Exposure

- Capturing motion
    1. Set the shutter speed that gives the right amount of blur/sharpness
    2. Find the aperture value that gives the right exposure
- Capturing still subjects
    1. Set the aperture value that gives the right depth of field
    2. Find the shutter speed that gives the right exposure
- Capturing multi-layered scenes
    1. Find the right aperture and shutter speed without flash for the
       background
    2. Find the flash setting that gives the right exposure of the foreground
       with the same aperture and shutter speed
- Capturing multi-layered scenes at night
    1. Find the right aperture and flash setting for the foreground
    2. Keep the shutter open
    3. Freeze the subject with the flash

## Parameters

- The shallower the depth of field
    - The longer the focal length of the lens
    - The closer the plane of focus to the camera
    - The wider the aperture

### Rules of thumb

- Aperture
    - _<= f/4_: shallow depth of field (portraits)
    - _>= f/16_: large depth of field (landscapes)
- Shutter speed
    - _<= 1/125_: frozen motion
    - _>= 1/60_: blurred motion
    - _<= 1/focal length[mm]_: suitable for handheld photos
    - _1/30 ... 1/4_: suitable for panning
- ISO
    - _<= 400_: low noise
    - _>= 800_: visible noise
