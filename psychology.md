# Psychology Notes

## Psyche (Freud)

- __id__: the instinctive mind (the source of needs, wants, desires and
  impulses)
- __ego__: the rational mind (the perception of "self")
- __super-ego__: the moral mind (the internalisation of cultural rules)

## Hierarchy of needs (Maslow)

1. _physiological_: food, shelter, sex
2. _safety_: personal, financial, health
3. _love & belonging_: family, friendship, intimacy
4. _esteem_: respect, self-confidence, freedom
5. _self-actualisation_: altruism, spirituality

## Five core needs (Koch)

- _security_: Who can I trust?
- _identity_: Who am I?
- _belonging_: Who wants me?
- _purpose_: Why am I alive?
- _competence_: What do I do well?

## Hierarchy of logical levels (Dilt)

1. _environment_: Where?
2. _behaviour_: What?
3. _competence_: How?
4. _beliefs_: Why?
5. _identity_: Who am I?

## Stages of psychological development (Erikson)

- _infancy_: hope (basic trust vs. mistrust)
- _early childhood_: will (autonomy vs. shame and doubt)
- _preschool age_: purpose (initiative vs. guilt)
- _school age_: competence (industry vs. inferiority)
- _adolescence_: fidelity (identity vs. role confusion)
- _early adulthood_: love (intimacy vs. isolation)
- _adulthood_: care (generativity vs. stagnation)
- _mature age_: wisdom (ego integrity vs. despair)

## Spheres of recognition (Honneth)

- _private sphere_: personal relationships; creates self-confidence
- _legal sphere_: rights to be regarded as equal members of the community;
  creates self-respect
- _solidarity sphere_: being part of a group; creates self-esteem

## Conditions of flow (Csíkszentmihályi)

- Clear goals (SMART)
- Immediate feedback
- Balance between challenges and skills

## Conditions of flow (Schaffer)

- Knowing what to do
- Knowing how to do it
- Knowing how well you are doing
- High perceived challenges
- High perceived skills
- Freedom from distractions

## Cognitive dissonance (Festinger)

- Attempting to hold two contradictory beliefs results in uncomfortable feeling
- The dissonance is resolved through one of the following:
    1. We change our beliefs to fit the new evidence
    2. We seek out new evidence that confirms our preferred belief
    3. We reduce the importance of the evidence that confirms our unpreferred
    belief

## The four temperaments (Hippocrates)

- __sanguine__: active, social, extraverted
- __choleric__: independent, goal-oriented, extraverted
- __melancholic__: perfectionist, detail-oriented, introverted
- __phlegmatic__: easy-going, sympathetic, introverted

## Type A&B personality theory (Friedman & Rosenman)

- __type A__ personality: ambitious, competitive, hasty, active, impatient,
  anxious, proactive, suffers from a sense of urgency, hates procrastination,
  can’t relax
- __type B__ personality: relaxed, reflective, gives himself time to think,
  patient, able to listen to others, with many hobbies and pleasures, not
  oriented on results, lacks clear goals, avoids conflicts

## Myers & Briggs type indicators

- _Introverted vs. Extraverted_: gains energy alone vs. in social situations
- _iNtuitive vs. Sensing_: focuses on what might happen vs. what has happened
- _Thinking vs. Feeling_: focuses on rationality vs. morality
- _Perceptive vs. Judging_: keeps options open vs. prefers clear decisions

## Givers & takers (Grant)

- __giver__: "What can I do for you?"
- __taker__: "What can you do for me?"
- __matcher__: "I'll do to you what you do to me."

## DOES attributes of highly sensitive persons (Aron)

- Depth of processing
- Overstimulated (over-aroused)
- Emotionally reactive (emphatic)
- Sensitive to subtle stimuli

## Attachment types (Bowlby & Ainsworth)

- __secure__
- __anxious__: nervous about intimacy; requires frequent reassurance and
  validation
- __avoidant__: uncomfortable with intimacy; rejects being open with emotions
- __disorganised / fearful avoidant__: fearful of intimacy; withdraws from
  relationships when they get serious

## Sexuality types (Sue Johnson)

- __synchrony sex__: balance between pleasure and emotional bonding; secure
  attachment
- __solace sex__: used for reassurance and to calm conflicts; anxious attachment
- __sealed off sex__: self-focused and performance oriented; avoidant attachment
- __squabble sex__: forceful and overly passionate; fearful avoidant attachment

## Conditions of friendship (Rebecca Adams)

- Repeated unplanned interaction
- Shared vulnerability
- Transition (people in times of transition in their lives are the most open to
  new friendships)

## Risk regulation theory (Murray, Holmes & Collins)

- _relationship-promotion goals_: closeness & interdependence
- _self-protection goals_: minimising hurt and rejection
- Which one someone prioritises in their relationships depends on whether they
  have positive or negative self-image

## Habit loop (Fogg & Duhigg)

- _cue_: the trigger
- _routine_: the action itself
- _reward_
- _craving_: anticipation

## Paradox of Choice (Schwartz)

- Having too much choice results in an inability to make a decision or being
  less satisfied with our decision
- People have the capacity to process 7 'bits' of information at one time
  (Miller)

### Satisficers vs. maximisers (Simon)

- __satisficer__ (satisfaction + sacrifice): makes choices by establishing a
  threshold of the acceptable levels of attributes, chooses the first option
  passing the threshold, and stops searching
- __maximiser__: strives to find the best, seeks to consider all possible
  options

## Types of power (Zigarmi)

### Illegitimate

- __reward power__: using the promise of compensation to make people conform
- __coercive power__: using threats and punishment to make people conform
- __referent power__: power based on people's desire to identify with the
  powerful; leads to emotional dependence

### Legitimate (justifiable)

- __reciprocity__: making people feel obliged to return you a favour
- __equity power__: making people feel that you expect compensation for your
  efforts
- __dependence power__: making people feel they are obliged to help you
- __expert power__: dependence on your expertise
- __information power__: abusing your ability of being persuasive

## Four tendencies (Rubin)

### Expectation types

- __outer__ expectation: e.g. work deadline, request from a friend
- __inner__ expectation: e.g. a new year's resolution

### Character types by how they respond to expectations

- __upholder__: readily meets outer and inner expectations
- __questioner__: does something if it makes sense; turns all expectations into
  inner expectations
- __obliger__: readily meets outer expectations, needs outer accountability to
  meet inner expectations
- __rebel__: resists all expectations

## Synthetic happiness theory (Gilbert)

- __natural happiness__: happiness by getting what we want
- __synthetic happiness__: happiness by changing our views of the world so that
  we feel good about a non-favourable situation; a result of unconscious
  cognitive processes
- Natural and synthetic happiness are equivalent
- __impact bias__: our tendency to overestimate the length and intensity of our
  future hedonic state as a result of an event (e.g. failing an exam, getting a
  promotion)

## AWARE method for overcoming anxiety (Beck)

1. _acknowledge_: don't try to escape the feeling
2. _wait_: observe yourself until you're ready to act
3. _act_: deep breathing, PMR, meditation, etc.
4. _repeat_
5. _end_: remind yourself that it is a temporary state

## Emotional competencies (Goleman)

- self-awareness
- self-regulation
- motivation
- empathy
- socialization

## Appraisal theory of forming emotions

- stimulus + interpretation + identification + repetition = strong emotion
- _interpretation_: your perception of an event based on your personal story
- _identification_: the attention you give to the feeling
- _repetition_: holding onto the feeling

## IMPROVE methods for regulating emotions (Linehan)

- _imagery_: visualising a peaceful setting
- _meaning in life_: purpose
- _prayer_: meditation
- _relaxation_: deep breathing, stretching, yoga, etc.
- _one thing in mind_: mindfulness
- _vacation_: break from routine
- _encouragement_: self-motivation

## Sedona Method of releasing emotions (Dwoskin)

Focus on a feeling and ask the following questions repeatedly:

1. Could I...
    - let this feeling go?
    - allow this feeling to be here?
    - welcome this feeling?
2. Would I?
3. When?

## Phases of anger (Twerski)

1. _anger_: the feeling itself
2. _rage_: the reaction to anger
3. _resentment_: hanging onto the feeling of anger

## Guilt vs shame (Brown)

- _guilt_: I did bad (behaviour)
- _shame_: I am bad (identity), as a result unworthy of love or belonging

## Types of anger (Máté)

- _healthy anger_: emotional defence mechanism to boundaries being violated at
  the moment; proportional to the triggering stimulus
- _unhealthy anger_: hurt being triggered from the past; disproportionate to the
  triggering stimulus

## A-B-C-D model of anger management (Ellis)

1. _activating situation_: that triggers anger
2. _belief system_: how the situation is (mis)interpreted
3. _consequences_: feelings & physiological effects
4. _dispute_: examining behaviour and reinterpreting the situation

## The five stages of grief (Kübler-Ross)

1. denial
2. anger
3. bargaining
4. depression
5. acceptance

## Acceptance and commitment therapy (Hayes)

### FEAR (negative approach)

- Fusion with your thoughts
- Evaluation of experience
- Avoidance of your experience
- Reason-giving for your behavior

### ACT (positive approach)

- Accept your reactions and be present
- Choose a valued direction
- Take action

## The four shame responses (Rubin & Lyon)

1. Attacking others ("You're toxic")
2. Attacking yourself ("I'm toxic")
3. Denial (dissociation)
4. Withdrawal (isolation)

## Nonviolent communication (Rosenberg)

### Violent communication ("you" statements)

1. _observation_: what's happening
2. _interpretation_: what I _think_ about _you_ (e.g. "You did that because
   don't love me any more")
3. _reaction_: aggression
    - Judgement
    - Demand
    - Criticism
    - Expectation
    - Analysis
    - Diagnosis
    - Advice

### Nonviolent communication ("I" statements)

1. _observation_: what's actually happening
2. _feelings_: Identify _my_ emotional response
3. _needs_: Figure out the unmet need that is driving that feeling
4. _request_: Explicitly _ask for_ (don't _demand_) what I need

### The four 'D's of disconnection

- Diagnosis (i.e. judgements, criticism, comparison)
- Denial of responsibility
- Demands
- Deserve-oriented language

### Meaningful appreciation

Appreciation only feels genuine if it's based on:

- Actions that have contributed to our well-being
- Needs of ours that have been fulfilled
- Pleasureful feelings caused by the fulfilment of those needs

## Reflective listening (Rogers)

### DOs

- Show positive body language
- Summarise what your partner said using their own words ("So what you're saying
  is…", "So in other words…")
- Confirm that your partner is understood by reflecting their _feelings_ back
- Assure your partner that they're not alone

### DON'Ts

- Beg the question (e.g. "How are you" over "Are you OK")
- Give advice, give your personal interpretation, or offer a solution
- Bring up your own similar experiences
- Analyse or trivialise the problem
- Lapse into silence, letting the other do a monologue
- Repeat the same phrases
- Pretend understanding when you get lost. If you find it difficult to focus,
  repeat the other's words in your head
- Say or do anything that distracts from the topic

### Examples

- Statement: "I don't like the coat you're wearing."; Reply: "You really dislike
  this coat."
- Statement: "I hated living in that house."; Reply: "It was pretty bad there
  for you."
- Statement: "Why is it always me?"; Reply: "It doesn't sound fair, does it?"

## Assertive communication (Gordon)

### Types of communication

- __passive__: You are more important than I am
- __aggressive__: I am more important than you are
- __assertive__: We are both equally important

### DESC script for assertiveness

- Describe what you experience ("observation" in NVC)
- Explain its effect on you ("feelings" in NVC)
- Show that you understand the effects ("needs" in NVC)
- Communicate your proposed solution ("request" in NVC)

### Practical assertiveness

- Use a firm but polite tone
- Say "no" when you mean "no"
- Be factual, don't judge or generalise; avoid "always" and "never"
- Use _"I" statements_ instead of _"you" statements_ (e.g. "I disagree" instead
  of "you're wrong")
- Focus on the effects of your partner's behaviour on you: "When you [do
  something] then [something happens], and I [feel something]."

## Mindful communication (Thich Nhat Hanh)

### The four guidelines of right speech

1. Tell the truth.
2. Don't exaggerate.
3. Be consistent. (This means no double-talk.)
4. Use peaceful language.

### The four criteria of mindful teaching

1. We have to speak the language of the world.
2. We may speak differently to different people, in a way that reflects how
   they think and their ability to receive the teaching.
3. We give the right teaching according to person, time, and place.
4. We teach in a way that reflects the absolute truth.

### The six mantras of loving speech

1. "I am here for you."
2. "I know you are there, and I am very happy."
3. "I know you suffer, and that is why I am here for you."
4. "I suffer, please help."
5. "This is a happy moment."
6. "You are partly right."

## Dealing with angry people

- Respond calmly; fighting fuels anger. If you can't, flee
- Ask questions empathically to find the cause. Use reflective listening
- Understand that it isn't your fault, in order to distance yourself
  emotionally; it helps you keep your calm (Blechert)
- If it's your fault then apologise, don't be defensive; defensiveness fuels
  anger
- Try to distract the person instead of ruminating on the problem; rumination
  fuels anger. Try to turn anger into laughter
- There is direct correlation between perceived powerlessness and ensuing
  aggression

## Funnel technique of questioning

- Redirect attention gradually from generic to specific
    1. Open prompts ("Tell me more about X")
    2. Focusing questions ("How could you clarify X?")
    3. Linking questions ("What did you mean by X?")
    4. Challenging questions ("Have you talked to Y about it?" / "Do you think Y
    did it deliberately?")
    5. Closing questions ("Is there anything else you'd like to tell about X?")
- Goal: extracting _needs statements_ from blaming comments

## Conflict resolution

- 3-step process
    1. Calm down (deep breating, walk, bath, contemplation etc.)
    2. Share each perspective (no discussion, just listen, take notes)
    3. Discuss, negotiate and compromise
- Take a break between each phase, sleep on it if possible

### Resolution styles

- __passive__: I lose, you win
- __aggressive__: I win, you lose
- __assertive__: I win, you win

## Manipulation

- Persuasion is manipulative when one tries to get the other to adopt what
  they themselves regard as a false belief (Robert Noggle)
- __sarcasm__: belittling someone by taking what they said out of context and
  exaggerating it to the point where it appears stupid or inane
- __invalidation__: deliberately not acknowledging what someone says
- __blame shifting__: responding to an accusation with accusation
- __projection__: accusing someone of things that the manipulator actually does
- __word salad__: jumping from topic to topic to distract from the original
  topic
- __hurt & rescue__: hurting someone and immediately coming up with a solution
  that makes the manipulator look superior
- __gaslighting__: encouraging someone to doubt their own judgment and to rely
  on the manipulator’s advice instead
- __guilt trip__: making someone feel guilty about failing to do what the
  manipulator wants them to do
- __charm offensive__: inducing someone to care so much about the manipulator’s
  approval that they will do as the manipulator wishes
- __impostor syndrome__: when someone doubts their accomplishments, and has a
  persistent internalised fear of being exposed as a fraud (vs. __Dunning–Kruger
  effect__: when someone who is incompetent overestimates their accomplishments)

## Responses to manipulative abuse (Michele Lee Nieves)

- Response to blame shifting & projection:
    - "Thank you for letting me know how you feel."
    - "I just wanted you to know how I felt."
    - "I am sorry you feel that way."
- Response to word salad:
    - "I hear you."
    - "Thank you for expressing your viewpoint."
- Response to gaslighting & guilt trip:
    - "I choose to see things differently."

## Dependency disorders

- __projection__: attributing feelings someone has about another person to that
  person, and imagining that those feelings are projected back at them
- __transference__: redirecting feelings someone has about a person to a
  different person based on superficial similarities
- __enmeshment__: a relationship in which personal boundaries are so unclear
  that one owns the other’s emotions
- __codependence__: a relationship in which one obsessively needs to take care
  of the other
- __dependent personality disorder (DPD)__: an excessive need to be taken care
  of and validated by others
- __borderline personality disorder (BPD)__: a pattern of swinging moods,
  self-image and behaviour; e.g. impulsiveness (rage and self-harm), a fear of
  abandonment, swinging between extreme closeness and extreme dislike

## The "four horsemen of relationship apocalypse" (Gottman)

- Criticising your partner's character ("You're so stupid" vs. "That thing you
  did was stupid")
- Blame shifting ("I wouldn't have done that if you weren't late all the time")
- Contempt (making your partner feel inferior)
- Stonewalling (withdrawing from an argument and ignoring your partner)

## The main issues couples argue about (Linda Blair)

- Imbalance of power or lack of reciprocity
- Lack or loss of trust
- Lack or loss of respect
- Differing needs for space and independence

## Theories of violence (Rai)

- __disinhibition theory__: everyone has violent impulses they can control most
  of the time; self-control occasionally breaks down because of exhaustion or as
  a response to provocation
- __rational theory__: violence is a way of achieving goals; the intent is
  gaining power
- __moral theory__: people are violent because they feel that their violence is
  a moral right or obligation in a certain situation; the intent is regulating
  social relationships according to a moral order

## Persuasion techniques

- __but-you-are-free__: when you make a request, emphasise the other person's
  ability to choose otherwise ("Could you take out the garbage? But it's OK if
  you don't.")
- __foot-in-the-door__: make a large demand after the other said yes to a small
  one
- __door-in-the-face__: make a small demand after the other said no to a large
  one
- __that's-not-all__: make a large demand and immediately, before the other has
  a chance to respond, lessen it to a smaller demand
- __low-ball__: make an initial offer that looks attractive, and only reveal the
  full details after the other said yes
- __social proof__: when you make a request, refer to others who have already
  said yes
- __nudge__: the way options are presented affect people's choices, e.g. most
  choose the second cheapest option to avoid looking stingy

## Prejudice

- __prejudice__: a preconception that cannot be falsified without emotional
  resistance (Allport)
- __availability heuristic__: our decisions are based on the most readily
  available information, not on accurate information
- __representativeness heuristic__: we judge two people as similar based on
  superficial similarities
- __confirmation bias__: we assume that every piece of evidence that confirms
  our belief is a general proof, while the ones that contradict it are fake or
  one-off exceptions to the rule
- __halo effect__: we take one aspect of someone's personality as a proxy for
  their overall character
- __pluralistic ignorance__: we assume that even though others' behaviour is
  similar to ours, their behaviour reflects different feelings or beliefs from
  ours
- __individuation__: when we form close relationship with someone from an
  out-group, we attribute all their positive traits to the individual, and not
  to the group
- __contact hypothesis__: the more people we contact from an out-group, the more
  we generalise the experience to the entire group
- __extended contact hypothesis__: knowing that a member of the in-group has
  close relationship with someone from an out-group reduces our prejudice
  towards the entire group
- __recategorisation__: shared identity is achieved by redefining the groups
  themselves to be more inclusive (e.g. fans of a sports team)
- __diffusion of responsibility__: we are less likely to take responsibility for
  action or inaction when others are present, because we assume that they are
  responsible, or already acted (special cases are _social loafing_ and
  _bystander effect_)
- __aversive racism__: unconscious discrimination by people who otherwise
  maintain an egalitarian self-image, caused by biases that only manifest in
  "edge" situations (e.g. hiring someone from a group of equally qualified
  candidates)
- __shifting baseline syndrome__: we assume that the circumstances we were born
  into are the standard, and we measure everything relative to these standards
  (Pauly)
- __complex contagion theory__: we have to be exposed to a new belief or
  behaviour from multiple sources before we adopt it
- __groupthink__: occurs when people maintain harmony and conformity inside a
  close-knit group by enforcing uniformity and isolating themselves from outside
  influences (Janis)
- __selective empathy__: we are only capable of feeling empathy towards
  individuals we are connected to, but not towards a group

## Anti-prejudice interventions (Wing Shieh)

- __contact__: prejudice can be reduced by encouraging members of two different
  groups to meet
- __awareness__: prejudice can be reduced by improving people’s knowledge and
  understanding of prejudice and discrimination, and the errors inherent in
  stereotypes and other common assumptions about others
- __categorisation__: reducing people’s prejudice by changing their own
  self-categorisation from a more narrowly defined group identity to a broader,
  more inclusive group
- __perspective-taking__: reducing prejudice by encouraging people to draw
  parallels between their own experiences and the experiences of those in other
  groups
- __social norm__: challenging people’s ideas about what is normal and
  acceptable, such as through community role models who exhibit non-prejudicial
  attitudes
- __perceived variability__: increasing people’s perception of the variability
  between individual members of a group

## Backfire effect (Gimbel & Harris)

- Providing more evidence to challenge someone's _core beliefs_ makes it less
  likely that they change their mind
- People interpret these arguments the same way as physical threat
- Arguments should be _reframed_ to reflect the worldview of those we'd like to
  convince

## Illusion of explanatory depth (Sloman & Fernbach)

- We rate our depth of knowledge higher than it actually is because we think we
  know how something is even if we are unable to explain it
- We have difficulties making a difference between our own knowledge and the
  people's we cooperate with
- __Dunning-Kruger effect__: the more shallow our knowledge about something, the
  more likely we overestimate our competence in it

## Types of isolation (Holt-Lunstad & Smith)

- __social isolation__: having few social connections or interactions; doesn't
  imply low perceived quality of life
- __loneliness__: the subjective perception of isolation, i.e. the discrepancy
  between the desired and actual quantity and quality of social connections

## Austrian schools of psychotherapy

- __psychoanalysis__ (Freud): life's driving force is will to pleasure
- __individual psychology__ (Adler): life's driving force is will to power (or:
  bringing inferiorities to an end)
- __logotherapy__ (Frankl): life's driving force is will to meaning

## Logotherapy (Frankl)

- Search for meaning is the primary motivation in life
- Mental health is based on a tension between what someone has achieved and what
  someone still ought to accomplish
- Meaning is
    - given by a cause to serve or a person to love
    - to be found in the world, not through introspection
    - specific to a person's life at a given moment
- Meaning can be discovered by
    - creating a work or doing a deed
    - experiencing something or encountering someone
    - finding purpose in unavoidable suffering
    - prerequisite: redirecting attention away from the self

### Types of neurosis in logotherapy

- __hyper-intention__: an excessive desire for some event to happen, which makes
  that event less likely to happen
- __hyper-reflection__: an excessive attention on the self as an attempt to
  avoid neurosis, which makes the percieved neurosis even stronger
- __anticipatory anxiety__: the fear of an undesired outcome, which makes that
  outcome more likely to happen

## Self-expansion model (Aron & Aron)

- The primary motivation in life is to self-expand
- Self-expansion can be achieved through close relationships that lead to the
  inclusion of the other's resources (social networks), perspectives and
  identities in the self
- The desirability of a partner is judged upon the potential amount of
  self-expansion possible from the relationship

## Meaning in life (Susan Wolf)

- Meaning in life comes from being absorbed in activities that we find
  _objectively_ good
- Meaning in life arises when subjective attraction meets objective
  attractiveness

## Moravec's paradox (Moravec, Brooks & Minsky)

- Human reasoning (e.g. playing chess, speech) is easy to replicate with AI
- Perception and mobility (skills below the level of conscious awareness) are
  very difficult to replicate with AI
- Skills acquired by babies are the hardest of all human skills

## Three dimensions of self (Pál Ferenc)

- Individual self
- Communal self
- Universal self
- Awareness of all 3 are required for self-knowledge
