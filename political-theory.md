# Political Activism Notes

- __community__: a group of people united by at least one common characteristic,
  including geography, identity or shared interests

## Main ideologies

- __liberalism__: accentuates the right of individuals to freely engage in
  mutually profitable relationships; addresses the lack of freedom
- __socialism__: focuses on the struggles of dominated groups against dominating
  groups; addresses the lack of equality
- __conservatism__: accentuates ties of tradition; addresses the lack of
  security
- __nationalism__: accentuates ties of shared identity; addresses the lack of
  community
- __libertarianism__: accentuates the right to individual choice and judgement;
  addresses the lack of autonomy
- __anarchism__: rejects all kinds of hierarchy; addresses the lack of power

## Political positions

- __politics__: the process that determines how power is going to be used
  (Nathan Robinson)
- __value-based politics__: one's political position is based on ideals and
  ideological perspectives
- __identity politics__: one's political position is based on the interests of
  groups one identifies with, e.g. age, gender, ethnicity, social class

## Democracy

- __representative democracy__: elected representatives make decisions,
  their power is held in check by some kind of constitution; generates
  oligarchic rule (Aristotle) and aristocratic rule (Montesquieu)
- __direct (base) democracy__: people are directly involved in decisions
- __popular assembly__: councils formed of members of local communities make
  decisions that are handed up to upper-level councils of representatives in a
  bottom-up manner (Murray Bookchin)
- __sortition__: a form of popular assembly where assemblies are formed of
  representative random samples of people

## Paradigms of sociology

- __structural functionalism__ (Émile Durkheim): society is a system of _social
  structures_ that serve _social functions_ that work together to promote
  stability and social order; sees change as disrupting to society
- __conflict theory__: society is composed of groups that conflict over scarce
  resources; sees change as fundamental to society
    - __class conflict__ (Karl Marx): inequalities between conflicting classes
      based on their relationships to the means of production
    - __race conflict__ (W.E.B. Du Bois): inequalities between ethnic groups
    - __gender conflict__ (Simone de Beauvoir): inequalities between women and
      men
- __symbolic interactionism__ (Max Weber): the meaning people attach to
  interactions and social situations leads to a shared reality between members
  of a social group

## Branches of sociology

- __positive theory__ (Auguste Comte): a theory that is objective, fact-based
  and value-free; applies the scientific method
    - __positivist sociology__: the study of society based on observations and
      measurements of social behaviour; based on __quantitative research__: the
      study of measurable data using statistical methods
    - __interpretive sociology__: the study of society that focuses on the
      meanings that people attach to their social world; based on __qualitative
      research__: study using descriptions and illustrations (interviews,
      questionnaires, first-hand observations)
- __normative theory__: a theory that is subjective and value-based
    - __critical theory__: the study of society that focuses on the need for
      social change

## Activism

- __grassroots__: a movement originating from the people, popularised through
  person-to-person communication
- __astroturfing__: paying people of the public to appear to be supportive of a
  case, faking a genuine grassroots movement
- __hashtag activism__: confusing expressions of support for a movement with
  taking action
- __radical flank effect__ (Jo Freeman): a radical, violent offshoot of a
  movement increases the popularity of the mainstream movement

## Critical theory (Frankfurt School)

- __traditional theory__: derives generalisations as laws about the world, the
  method of science
- __critical theory__: social sciences cannot imitate traditional theory because
  ideologies shape the researchers' thinking; changing society for the better
  instead of purely describing it is a duty of social scientists
- __critique__: reflection on the validity and limits of a claim or condition
  (Immanuel Kant)
- For a theory to be considered a critical theory, it should be
    - _explanatory_: describes an existing social problem
    - _practical_: offers a feasible solution
    - _normative_: withstands criticism

## Principle of Liberty (John Stuart Mill)

- The only rightful purpose for exercising power against someone's will is to
  prevent harm to others
- _Harm_ may be an act of _omission_ (e.g. failing to help someone who is injured) or
  _comission_ (e.g. injuring someone)
- Someone's own good is not a rightful purpose

## Features of capitalism

- __commodification__: capitalism regards natural resources, labour,
  information etc. as commodities that can be accumulated and traded
- __market economy__: in a market economy all goods and services are distributed
  strictly by means of market competition
- __absentee ownership__ (Thorstein Veblen): capitalists generate income by
  renting out property that they don't reside on and by owning businesses that
  they don't run themselves
- __cycle of accumulation__ (Karl Marx): capitalists invest all money into
  commodities that generate profit that is reinvested into more commodities ad
  infinitum, symbolised with the formula `M-C-M'`
- __boom and bust cycle__ (Karl Marx): capitalists maximise the exploitation of
  resources (_expansion_), which leads to resource depletion (_contraction_),
  after which they switch to new resources to exploit
- __growth imperative__: in a capitalist economy constant growth is necessary
  to avoid instability, either because of market competition (Karl Marx), or
  because the capitalist economy is over-reliant on credit money that needs to
  be repaid to banks on interest (H.C. Binswanger), or because growing economic
  output is a prerequisite of low unemployment (Art Okun)
- __commodity fetishism__ (Karl Marx): for capitalists every commodity has
  intrinsic market value detached from the social costs of its production
- __hidden abode of production__ (Karl Marx): capitalists hide the conditions
  of production from consumers' view

## Possessive individualism (C.B. Macpherson)

- The human essence is to use our capacities in search of our satisfactions
- Society is a lot of free equal individuals related to each other through their
  possessions
- Political life is about the protection of those possessions
- All capacities including life and liberty are considered possessions rather
  than social rights and obligations, the rights to the use of property are thus
  fundamental
- Society values above all the right to acquire as much property as possible
- The above leads to an infinite desire to possess and consume

## Tragedy of the commons (W.F. Lloyd, Garrett Hardin)

- Individual players acting independently according to their self-interests
  eventually deplete or spoil the shared resources they all depend on
- No-one takes responsibility for things that everybody owns
- People always question the authority of those who attempt to regulate the
  commons, which recreates the tragedy again and again
- The solution is "mutual coercion that is mutually agreed upon" by all players
- vs. __invisible hand__ (Adam Smith): individual players acting according to
  their self-interests in a well-functioning system of mutual interdependence
  (_perfect competition_) produce unintentional widespread benefits
- __Malthesian catastrophe__ (Thomas Malthus): mass famine that leads to the
  return to a sustainable level of population when population outgrows the
  capacity of agricultural production
- __The Limits to Growth__ (Club of Rome): a report of a mathematical simulation
  of population and economic growth, forecasts a Malthesian catastrophe between
  2030 and 2050

## Technofeudalism (Yanis Varoufakis)

- Markets have been replaced by platforms controlled by obscure algorithms that
  vendors pay rent to
- Capital has been replaced by data that platforms use to control consumer
  behaviour
- Profits have been replaced by the prospect of achieving power over data as
  the measure of a company’s value
- Made possible by handouts of central bank money to financial firms since the
  2008 crisis

## "Burden of proof" definition of anarchism (Noam Chomsky)

- Anarchism is the point of view that no authority or domination is
  self-justifying; they all have a burden of proof to bear
- Any authority has to demonstrate that it's legitimate; if it cannot then it
  has to be dismantled
- It is the responsibility of those who exercise authority to demonstrate their
  legitimacy
- It is the responsibility of those in subordinate position to challenge those
  who exercise authority

## Branches of anarchism

- __individualist / market anarchism__: advocates private property without state
  protection
    - __anarcho-capitalism__: "laissez-faire" free market capitalism
    - __geo-anarchism__: market economy where all land is owned by locals
    - __mutualism__: market economy governed by reciprocity where all
      organisations are owned by their members (_cooperatives_)
- __social anarchism__: advocates collective ownership without state control
    - __anarcho-syndicalism__: society is organised around workers' unions of
      shared ownership, preparing a transition to collectivism or communism
    - __anarcho-collectivism__: society is made up of small-scale autonomous
      communities of shared ownership
    - __anarcho-communism__: collectivism where money is abolished, individuals
      contribute according to their abilities and take freely from the commons
      according to their needs

## Property vs. possession (Pierre-Joseph Proudhon)

- __property__ ("use and abuse" / "sticky" property): the power over a thing
  through coercion, e.g. a husband is a proprietor
- __possession__ ("occupation and use" property): inherent in human nature, e.g.
  a lover is a possessor

## Mutualism (Pierre-Joseph Proudhon, Josiah Warren)

- Property by coercion (_absentee ownership_) is theft; the _right to means_ is
  common (shared)
- Property by labour is legitimate; the _right to product_ is exclusive
- All work is equal, i.e. the just compensation for labour or its product is an
  equivalent amount of labour or its product; therefore rent and wages are
  immoral (_labour theory of value_)
- Business is conducted through voluntary contracts between people and groups,
  without external regulation

## Mutual aid (Peter Kropotkin)

- Reciprocal non-monetary exchanges made in a system of voluntary cooperation
  for mutual benefit
- Based on solidarity (that is horizontal) and not charity (that is vertical)
- Recognises that no act is fully egoistic or altruistic, but concerns both the
  self and the others

## Communal individuality (Alan Ritter)

- Individualism and collectivism are two means to the same end
- Flourishing of the individual is only possible in a liberated society of
  equals
- The autonomy of one is the precondition for the autonomy of all
- The struggles for individual freedom and for social justice are the same

## Double movement (Karl Polanyi)

- _first movement_: free-market liberals remove society's rules from the market,
  and instead subject society to the rules of the market
- _second movement_: reaction against the first movement to mitigate the damage
  on society, e.g. welfare, poorhouses
- __business ontology__ (Mark Fisher): the idea that everything in society,
  including healthcare and education, should be run as a business

## Theories of exploitation

- __distribution theory__ (John Roemer): A exploits B if A acquires more
  resources and B acquires fewer resources than they would in an equal society,
  and A acquires the excess on the expense of B
- __power theory__ (Nicolas Vrousalis): A exploits B if A is profiting from a
  transaction with B in which the power structure is unequal

## Pillars of sustainability (Agenda 21, Rio Earth Summit)

- _social_: protecting cultural heritage
- _environmental_: preserving the physical environment
- _economic_: well-being of communities
- __sustainable development__: human development that takes all 3 into account
  because they are inter-dependent (one cannot be achieved without the other)
- __doughnut economics__ (Kate Raworth): a visual model of economics that
  defines a sustainable range between the social minimum and the ecological
  limits

## Liberal democracy (Yascha Mounk)

- __democratic system__: a set of institutions that translates popular views
  into public policy
- __liberal system__: a set of institutions that protect individual rights
  (freedom of speech, press, faith etc.)
- __liberal democracy__: a political system that is liberal and democratic

## Deliberative democracy (Joseph M. Bessette)

- __consensus decision-making__: decisions are based on a general agreement,
  reached through deliberation
    - __agreement__: support for a proposal
    - vs. __consent__: lack of objections; cooperation without real support
- __elite deliberative democracy__: decisions are made through deliberation by
  elected members of decision-making bodies
- __populist deliberative democracy__: decisions are made through deliberation
  by a group of empowered citizens
- __elite deliberation__: decisions are made through deliberation by unelected
  people in power

## Types of populism (Robert Reich)

- __authoritarian populism__: a strongman is entrusted with fixing the system on
  behalf of the people; often uses scapegoating instead of actual reforms
- __reformist populism__: builds a new system that is more reflective of
  people's needs, with the people's involvement

## Requirements of representative democracy (Robert Dahl)

- Free, fair and competitive elections
- Full adult suffrage
- Protection of civil liberties
- Absence of unelected authorities that limit elected officials' power

## Requirements of representative democracy (Brian Klaas)

- Fair elections
- Separation of powers
- Rule of law
- Free press
- Accountability of elected officials

## The 3 pillars of livability (Luca Calafati et al.)

- Disposable and residual income
- Essential services
- Social infrastructure

## Features of well-functioning markets (John McMillan)

- Information flows smoothly
- People can be trusted to live up to their promises
- Competition is fostered
- Property rights are protected, but not overprotected
- Damaging side effects on third parties (e.g. the natural world) are curtailed

## Induced demand (Stanley I. Hart & Alvin L. Spivak)

- Increased supply of a commodity lowers its cost and leads to increased demand
- vs. __reduced demand__: decreased supply implies decreased demand
- An example is __traffic generation__: increasing road capacity leads to
  greater traffic as the higher throughput convinces more people to drive a car

## Policy-making

- __Hawthorne effect__: people change their behaviour in the presence of an
  observer, which makes objective sociological observations difficult
- __Overton window__: the range of policies acceptable to the mainstream
  population at a given time
- __Goodhart’s Law__: when a measure becomes a target, it ceases to be a good
  measure, as people switch to optimising for whatever is measured rather than
  putting their best efforts into actually doing good work
- __Campbell’s Law__: the more any quantitative indicator is used for
  decision-making, the more subject it will be to corruption, and the more it
  will distort and corrupt the processes it is intended to monitor
- __cobra effect__: an attempted solution to a problem makes the problem worse,
  as unintended consequence
- __Jevons paradox__: when a policy increases the efficiency of resource usage,
  but the demand for that resource rises so much that the total consumption
  exceeds the consumption at lower efficiency

## Greenwashing

- __greencrowding__: when a company joins a green alliance but then ignores or
  lobbies for lowering its climate commitments
- __greenlighting__: when a company focuses all of its advertising on its
  climate commitments however small they are
- __greenshifting__: when a company shifts responsibility from producers to
  consumers (e.g. BP's carbon footprint campaign)
- __greenlabelling__: when the advertisement or packaging of a product implies
  that the product is sustainable but it actually isn't
- __greenrinsing__: when a company continuously lowers their climate commitments
  until they can be comfortably met
- __greenhushing__: when a company makes it difficult or impossible to verify
  their climate commitments (e.g. when an investment bank claims that their
  private funds are ethical)

## Critical pedagogy (Paulo Freire)

- __praxis__: the cycle of theory (i.e. careful planning) → action → reflection
- __domesticating (banking) education__: an all-knowing teacher pours knowledge
  into the minds of controllable objects
- __liberating education__: the practice of freeing minds through a culture of
  questioning
- __cultural invasion__: the colonising group imposes values on the colonised
  group the colonised assimilates with, and so they become alienated from their
  own culture
- __dehumanisation__: the oppressor strips the oppressed of their dignity (e.g.
  "benefit scroungers") in order to maintain the structure of power
- __conscientisation__: the oppressor makes the oppressed question their own
  perception of reality and internalise the oppressor's imposed reality
  (_gaslighting_)
- __magical consciousness__: the oppressed are in a passive unquestioning state
  about the injustices in their lives
- __naïve consciousness__: the oppressed are aware of injustices but they cannot
  link them to structural discrimination and blame themselves
- __critical consciousness__: the ability to connect personal experiences to the
  political context
- __false generosity__: empty gestures of benevolence that pathologise the
  oppressed's situation instead of politicising it
- __culture circles__: dialogue groups in which a liberating learning context is
  created
- __animator__: the mediator of a culture circle who sets the context without
  using any kind of power
- __narrative__: an everyday story from people's lives
- __generative theme__: a common theme in people's narratives that generates
  passion and motivation to act
- __codifications__: scenes of everyday life that help people identify
  generative themes
- __dialogue__: a mutual learning experience between the animator and the
  members of the culture circle
- __collective action__: acting on a generative theme

## Hegemony (Antonio Gramsci)

- __coercion__: state control through the law, police and armed forces
- __ideological persuasion__: making people consent to the social order by
  embedding imposed attitudes and values in cultural institutions and education
- __traditional intellectual__: an educated person who has a social conscience
  that is bigger than their loyalty to their social class
- __organic intellectual__: a person from any social group who has the capacity
  to lead social transformation; having organic intellectuals in oppressed
  groups is a prerequisite of their liberation

## Ideologies (Hannah Arendt)

- Are divorced from lived experience and foreclose the possibility of new
  experience
- Are concerned with controlling and predicting the tide of history
- Do not explain what _is_, they explain what _becomes_
- Rely on procedures in thinking that are divorced from reality
- Insist upon a "truer reality" beyond perceptible things

## Radical therapy (The Radical Therapist magazine)

- People are inherently good and mental suffering is not an illness
- Suffering is rooted in our social relationships and it has two components:
    - Suffering from oppression (can be tackled by activism)
    - Suffering from alienation (can be tackled by therapy)
- Psychotherapy should aim for both personal and social change

## Radical transparency

- The principle of having open access to all information by default within an
  organisation
- All decision making is carried out publicly
- Every case when a piece of information doesn't have open access has to be
  justified

## Ladder of citizen participation (Sherry Arnstein)

1. Nonparticipation
    - __manipulation__: e.g. advisory committees
    - __therapy__: e.g. public hearing
2. Tokenism (fake/false participation)
    - __informing__: e.g. pamphlets, staff announcement meetings
    - __consultation__: e.g. employee engagement survey
    - __placation__ (conciliation / moderation): e.g. citizens' committees,
      black history month
3. Empowerment
    - __partnership__: e.g. labour union
    - __delegation of power__: e.g. community budget
    - __citizen control__: e.g. kibbutzim of Israel, communes of Rojava

## Autonomous Administration of North and East Syria, Rojava

- Popular assembly system
    1. _Commune_: comprises of 30-400 households
    2. _Neighbourhood Council_: represents 7-30 communes
    3. _District Council_: represents all neighbourhood councils of an area
    4. _People's Council_: 11 people from each district council
- Committees
    - Exist in every district, cooperating with each other
    - Separate Women's Council
- Democratic self-administration
    - _Legislative Council_: parliament with 40% of seats going to the assembly
      and 60% to elected MPs of political parties (_dual power_)
    - _Executive Council_: elected government

## Non-places (Marc Augé)

- __anthropological place__: that has a history and that is concerned with
  people's identity, i.e. where people can form social ties over shared cultural
  heritage
- __non-place__: homogenised place where people are anonymous and lonely; e.g.
  hotel rooms, airports, shopping malls
- __third place__ (Ray Oldenburg): people's social environment other than their
  home and work, where they can meet people outside their closest ties, e.g.
  library, gym, pub, café, park; essential for social cohesion

## Empty signifiers (Ernesto Laclau)

- Symbols and concepts that are vague enough to mean many things to many people,
  but specific enough to identify with (e.g. nation, hope)
- Their content is insignificant, their point is to make people form identities
  around them
- Populist governments emphasise identity over policy, and they use empty
  signifiers to build shared identities
- An empty signifier makes it easy to form a homogenous group (e.g. members of a
  "nation" are equal in their national pride despite vast differences in wealth
  and power)

## Types of nationalism (Tamás Gáspár Miklós)

- __"classic" nationalism__: its goal is to establish national unity by forced
  assimilation of ethnic and cultural minorities (late 19th / early 20th
  century)
- __ethnicism__: its goal is to establish national purity by segregation of
  ethnic and cultural minorities (early 21st century)
    - ethnicists refer to "culture" as something inherent and unchangeable, i.e.
      they use culture as a synonym of race

## Features of fascism (Umberto Eco)

- Cult of tradition
- Rejection of modernism
- Distrust of the intellectual world
- Disagreement is treason
- Fear of difference
- Appeal to a frustrated middle class
- Obsession with an international plot
- Followers must feel humiliated by the wealth and force of the enemies
- Permanent warfare
- Contempt for the weak
- Cult of heroism
- Machismo (male elitism, misogyny)
- _selective populism_: individuals have no rights, the "People" is a monolithic
  entity expressing the "Common Will"
- _newspeak_: simplified language to limit the instruments for critical
  reasoning
- Denies all identities except the national identity and insists that I have
  obligations only towards my nation (Yuval Harari)

## Double consciousness (W.E.B. Du Bois)

- Members of an oppressed group are forced by the oppressors to maintain two
  identities (e.g. African and American) that they are prevented from merging
  into a shared identity
- They eventually give up their own culture in favour of the oppressors' culture
  (Frantz Fanon)

## Imperial boomerang (Michel Foucault)

- The concept that imperialist governments first trial repressive techniques
  in the colonies, then deploy them at home
- E.g. concentration camps, militarisation of the police, selling off public
  services to corporations

## Methods of despotism in fake democracies (Brian Klaas)

- Blurring the lines between truth and falsehood; this makes it difficult to
  ascertain who to trust in times of crisis
- Attacking the media for accurate reporting; to undermine the credibility of
  those who hold power to account
- Casting aspersions on the integrity of elections
- Politicising national security and using the "rally around the flag" effect to
  erode rights
- Policy changes are deliberately being obscured by moving at such a rapid pace
  of change that normal citizens can't keep up

## Methods of despotism (Erica Chenoweth)

### Strategies to reinforce elite royalty

- Purges in institutions
- Paying off an entourage
- Applying threats and bribes to make institutions conform

### Strategies to undermine opposition

- Counter-mobilising paid supporters
- Infiltrating the movement
- Expanding surveillance
- Using illegitimate laws to criminalise certain behaviours
- Imposing financial, administrative and legal burdens on NGOs
- Planting agents provocateurs to push the opposition into undisciplined
  behaviour

## Methods of oligarchy to preserve their power (Matthew Simonton)

- Paying off collaborators among common people to legitimise their power
- Controlling the public spaces to prevent people from organising
- Keeping people economically dependent on individual oligarchs
- Keeping secrecy in governance with selective messaging to targeted audiences
- Praising acts of philanthropy instead of successful public projects

## SLAPP (Penelope Canan, George W. Pring)

- _Strategic Lawsuit Against Public Participation_ a.k.a. _intimidation lawsuit_
- A series of lawsuits against activists or investigative journalists
- When the costs of defense become too high they have to abandon their
  opposition
- The accuser doesn't expect to win the lawsuits, their goal is imposing
  financial burden

## FLICC techniques of science denial (Skeptical Science)

- Fake experts
- Logical fallacies
- Impossible expectations
- Cherry-picking
- Conspiracy theories

## Techniques for manipulating public opinion (Sylvain Timsit)

- Shifting public attention from important issues to insignificant details
- Creating non-existent problems and then providing solutions
- Implementing an unacceptable measure in small steps over a long period of time
- Presenting an unacceptable measure as “painful but necessary”
- Treating people as babies in communication, using a linguistic style that
  doesn’t allow critical thinking
- Contacting people on emotion rather than logic
- Holding people in ignorance by preventing the lower classes from accessing
  quality education
- Encouraging the public to like mediocrity
- Discouraging rebellion by implanting feelings of guilt
- Knowing people better than they do themselves through accumulating data

## Moral panic (Stanley Cohen)

1. Someone or a group is perceived as a threat to social norms
2. The threat is depicted through over-simplified symbols in the media
3. The symbols rouse public concern
4. Authorities or policy makers respond, even if the threat is not real
5. The changes in policy lead to social changes

## Pollution paradox (George Monbiot)

- The more damaging an enterprise is, the more money it must spend on politics
  to ensure it's not regulated
- As a result, political funding is dominated by the most harmful companies
- They then wield the greatest political influence

## Types of activism (Roger Hallam)

- __individualist activism__: people with similar values form inward-focused
  groups and decide individually what actions they do or do not take part in
  based on how that action aligns with their conscience and interests
- __collectivist activism__: people with similar values and causes to fight for
  come together to form outward-facing groups and take action as collective
  entities; participants' identities are integrated with the groups'

## Classic activism (Jack Harich)

1. Identify the problem
2. Find _proper practices_ that would mitigate the problem
3. Tell people the truth about the problem and the proper practices (i.e. appeal
   to facts)
4. If that fails, try to inspire or bargain with people to adopt the proper
   practices (i.e. appeal to emotions)

### Problems

- Doesn't try to resolve the root cause of the problem
- Unable to tackle people's (and organisations') resistance to change

## Methodology of grassroots movements (Matthew Bolton, Citizens UK)

- You only get the justice that you have the power to make happen (Thucydides)
- Build up power by building a community through common self-interest (→
  _vision_)
- Break down the big case together into specific issues
- Identify who have the power to make the changes you need
- Take action to force a reaction from the people in power (instead of you
  reacting to the powerful's actions). If they don't implement the demanded
  changes, escalate the action (→ _tactics_)
- Start with winnable issues; build up incrementally to bigger issues,
  celebrating every small win (→ _strategy_)
- The "iron rule" of community development: _never do for others what they can
  do for themselves_

## Approaches to power (Matthew Bolton)

### Negative mindset

- Feeling completely powerless
- Choosing principled loss over pragmatic gain by rejecting compromise for the
  sake of pure idealism
- Believing that you have a monopoly on morality

### Positive mindset

- Everyone has some power that can be enhanced by using it strategically
- Compromises are necessary to achieve incremental change
- Take the powerful's interests seriously and respect their values

## Nonviolent social movements (Bernard Lafayette Jr.)

- Are open for anyone to join
- Are against certain forms of injustice and not certain people
- Originate from a local community (e.g. Montgomery); every participant can rely
  on the support of the community
- Receive support from outside the community (e.g. SCLC)
- Provide legal background (e.g. NAACP)
- Nonviolence is embedded in the groups' identity
- Participants have a sense of power
- Songs have distinguished role in creating community spirit
- Campaigns may be successful not because they succeed in making a moral appeal,
  but because it becomes unfeasible to fight them

## Nonviolent social movements (Erica Chenoweth)

- Nonviolent movements are twice as likely to succeed as violent movements
- The most reliable predictor of the success of a movement is the number of
  people it engages
- Violence deters people from joining
- __3.5% rule__: a movement is likely to succeed if it can actively engage 3.5%
  of the population

## Key considerations in nonviolent action (Gene Sharp)

- __pillars of support__: oppressors are dependent on the obedience of a number
  of key institutions and social networks; activists must identify them and
  determine which can be persuaded to withdraw their consent
- __negotiations__: once demands are set, they should not be changed; and every
  effort should be made on a settlement before escalating the action
- __openness__: being truthful and open about intentions and plans is crucial
- __timing__: surprise in nonviolent action can weaken its effectiveness; what
  counts is good timing
- __focus__: resistance should be concentrated on what is believed to be the
  opponent's weakest points
- __cause-consciousness__: justification of direct action should always be
  publicised
- __initiative__: the nonviolent group should control the course of action,
  instead of reacting to the opponent's initiatives
- __solidarity__: the participants need to feel constantly supported by the
  movement
- __repression__: the oppressor's acknowledgement of the challenge; the keys to
  overcome it are _persistence_ and _refusing submission_
- __education__: participants should receive advance training in nonviolence in
  order to prevent acts of violence
- __political jiu-jitsu__: the oppressor's repression (overreaction) can be used
  to alienate their supporters and turn third-parties against them

## Methods of nonviolent action (Gene Sharp)

- __protest__: raising awareness of the existence of the movement; mostly
  symbolic
- __noncooperation__: causing difficulties in maintaining the normal operation
  of the system; it's easier to make people refuse to do something that has been
  ordered than to make them do something risky
- __intervention__: direct challenge with the oppressor; requires high level of
  discipline

## Successful nonviolent direct action (George Lakey)

- Focuses on a _campaign_ (an escalating series of actions) instead of one-off
  reactive protests
- Links campaigns on an issue into a _movement_
- Has a _vision_ of what should replace the unjust status quo

## Gaining power (George Lakoff)

- Reacting to issues others have established keeps you powerless
- Gain power by
    - controlling the issues that are talked about
    - controlling the parameters of the space in which the issues are talked
      about

## Civil disobedience (Henry David Thoreau)

- Participants _willingly_ accept the consequences (the punishment for breaking
  the opposed law)
- They act upon a case bigger than themselves, not in their own self-interests
- Firm moral basis: against injustice, not against widely accepted social
  contracts

## Dilemma action (Roger Hallam)

- Direct action that makes authorities face a dilemma:
    1. They allow the action to continue, which encourages people to join
    2. They repress it, putting protesters into a morally superior position,
    which encourages people to join

## Spectrum of allies (Joshua Kahn Russel)

- The spectrum of actors consists of
    - active allies
    - passive allies
    - the neutral
    - passive opposition
    - active opposition
- A social movement must identify and engage passive allies, win over the
  neutral, and neutralise the passive opposition in order to succeed against the
  active opposition

## Finding a cause (Srdja Popovic)

- Assume that people are initially disinterested or hostile
- Draw a dividing line and list people who might support you with your cause on
  your side and those who don't on the other side; if the majority of the
  community is on your side and only the people in power are on the other side,
  the cause is justified
- A good cause has a _vision_ that gives supporters a collective identity
- A movement is working _towards_ a vision and not _against_ an enemy
- A good cause is _relevant_ to people; most people don't identify with abstract
  concepts like human rights

## Achieving change (David Harder)

1. Commit
2. Declare a vision or mission and share it with members of your tribe
3. Draw healthy attention to yourself (isolation leads to failure)
4. Build effective support systems (i.e. ask for help; everything of value is
   done through collaboration)

## Creative strategic planning (Walt Disney)

- Strategy is created in 3 subsequent stages, playing 3 different roles:
    1. the _dreamer_: How do we imagine an ideal solution?
    2. the _realist_: How can we realise the solution?
    3. the _critic_: What can go wrong?
- The room is divided into 3 parts, one for each role, separating the stages in
  space; it's possible to go back and forth between stages

## The basic laws of human stupidity (Carlo M. Cipolla)

1. Everyone underestimates the number of stupid people
2. The percentage of stupid people is the same in any group
3. Their social behaviour puts people into one of the following categories:
    - __intelligent__ people: whose actions produce gains to themselves and to
      others
    - __helpless__ people: whose actions produce gains to others but losses to
      themselves
    - __bandits__: whose actions produce gains to themselves but losses to
      others
    - __stupid__ people: whose actions produce losses to others without
      themselves deriving any gains
4. Dealing with stupid people is a mistake under any circumstances because they
   act irrationally
5. A stupid person is the most dangerous type of person, because the actions of
   a bandit only result in a transfer of welfare, while stupid people make the
   whole community worse off

### Implications on the performance of societies

- A country moving uphill has a high fraction of intelligent people who produce
  enough gains to the community to keep the constant fraction of stupid people
  at bay
- A country moving downhill has a growing number of bandits in power, which
  implies a growth in the number of helpless among those not in power; which
  arrangement strengthens the destructive power of the constant fraction of
  stupid people

## The 5 stages of denial (Dana Nuccitelli)

1. Deny that a problem exists
2. Deny that we're the cause
3. Deny that it's a problem
4. Deny that we can solve it
5. It's too late
